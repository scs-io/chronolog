//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    // if (BASKET_CONF->COMM_SIZE != 160) {
    //     printf("Test requires 160 processes\n");
    // }
    int num_events = 1000;
    size_t size_events_kb = 1000;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 1000);
    if(argc > 3) size_events_kb=std::strtol(argv[3], nullptr, 1000);

    CHRONOLOG_CONF->ConfigureChronologClient();
    
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();

    char journal_name[20];
    snprintf(journal_name, 20, "test");
    CharStruct j_name=CharStruct(journal_name);

    int status;

    // Shared journal test
    if(BASKET_CONF->MPI_RANK ==0){
        status = chronicle->Create(j_name);
        assert(status == 0);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK !=0){
        status = chronicle->Open(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(journal_name);
        events[i].event_id = GetTimeStamp();
        usleep(BASKET_CONF->MPI_RANK);
    }
    std::string data= random_string(size_events_kb*1024);

    Timer shared_timer=Timer();
    shared_timer.resumeTime();
    if(BASKET_CONF->MPI_RANK != 0) {
        for (int i = 0; i < num_events; i++) {
            status = chronicle->Append(events[i], data);
        }
    }
    shared_timer.pauseTime();

    double sum_shared_time;
    double local_shared_time = shared_timer.getElapsedTime();
    MPI_Reduce(&local_shared_time, &sum_shared_time, 1, MPI_DOUBLE, MPI_SUM,
               0, MPI_COMM_WORLD);
    if (BASKET_CONF->MPI_RANK == 0) {
        printf("Shared %d\n", sum_shared_time);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK != 0){
        status = chronicle->Close(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK == 0){
        status = chronicle->Destroy(j_name);
        assert(status == 0);
    }

    // journal per process test
    snprintf(journal_name, 20, "test_%d", BASKET_CONF->MPI_RANK);
    j_name = CharStruct(journal_name);

    status = chronicle->Create(j_name);
    assert(status == 0);

    MPI_Barrier(MPI_COMM_WORLD);

    Timer jpp_timer=Timer();
    jpp_timer.resumeTime();
    for (int i = 0; i < num_events; i++) {
        status = chronicle->Append(events[i], data);
    }
    jpp_timer.pauseTime();

    double sum_jpp_time;
    double local_jpp_time = jpp_timer.getElapsedTime();
    MPI_Reduce(&local_jpp_time, &sum_jpp_time, 1, MPI_DOUBLE, MPI_SUM,
               0, MPI_COMM_WORLD);
    if (BASKET_CONF->MPI_RANK == 0) {
        printf("Journal Per Process: %f\n", sum_jpp_time);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    status = chronicle->Destroy(j_name);
    assert(status == 0);

    /**
     * One hashmap for all chronicles VS hashmap per chronicle
     *
     * test case
     * 1K puts of 160 processes
     * 1. Put all puts to one journal (technically it will force all calls to go into one hashmap (like the shared example)
     * 2. Put all puts to journal per process (like the fpp example)
     *
     * Calculate the time elapsed overall.
     */

    free(events);
    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
