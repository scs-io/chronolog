//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>
#define ASSERT(left,operator,right) { if(!((left) operator (right))){ std::cerr << "ASSERT FAILED: " << #left << #operator << #right << " @ " << __FILE__ << " (" << __LINE__ << "). " << #left << "=" << (left) << "; " << #right << "=" << (right) << std::endl; } }
static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    // if (BASKET_CONF->COMM_SIZE != 160) {
    //     printf("Test requires 160 processes\n");
    // }
    int shared,num_events = 1000;
    size_t size_events_kb = 1000;
    bool is_shared = false;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 10);
    if(argc > 3) size_events_kb=std::strtol(argv[3], nullptr, 10);
    if(argc > 4) shared = std::strtol(argv[4], nullptr, 10);
    is_shared=shared==1;
    CHRONOLOG_CONF->ConfigureJournalClient();
    auto chronicle = basket::Singleton<journal::Client>::GetInstance();
    char chronicle_name[20];
    if(is_shared) snprintf(chronicle_name, 20, "test");
    else snprintf(chronicle_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(chronicle_name);
    int status;
    if(!is_shared || BASKET_CONF->MPI_RANK ==0){
        status = chronicle->Create(j_name);
        ASSERT(status, ==, 0);
    }
    // Shared journal test
    MPI_Barrier(MPI_COMM_WORLD);
    if(is_shared && BASKET_CONF->MPI_RANK !=0){
        status = chronicle->Open(j_name);
        ASSERT(status, ==, 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(chronicle_name);
        events[i].event_id = GetTimeStamp();
        usleep(BASKET_CONF->MPI_RANK);
    }
    std::string data= random_string(size_events_kb*1024);

    Timer record_timer=Timer();
    Timer playback_timer=Timer();
    for (int i = 0; i < num_events; i++) {
        record_timer.resumeTime();
        status = chronicle->Append(events[i], data);
        record_timer.pauseTime();
        ASSERT(status, ==, 0);
        Event e;
        e.journal_name_ = events[i].journal_name_;
        std::string tail_data;
        playback_timer.resumeTime();
        status = chronicle->Tail(e, tail_data);
        playback_timer.pauseTime();
        ASSERT(status, ==, 0);
        assert(tail_data == data);
    }

    double sum_record_time,sum_playback_time;
    double local_record_time = record_timer.getElapsedTime();
    double local_playback_time = playback_timer.getElapsedTime();
    MPI_Reduce(&local_record_time, &sum_record_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&local_playback_time, &sum_playback_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (BASKET_CONF->MPI_RANK == 0) {
        sum_record_time=sum_record_time/(1000.0*BASKET_CONF->COMM_SIZE);
        sum_playback_time=sum_playback_time/(1000.0*BASKET_CONF->COMM_SIZE);
        printf("#Procs,#Events,SizeEvents(KB),IsShared,Record Time(s),Record Throughput(E/s),Playback Time(s),Playback Throughput(E/s)\n");
        printf("%d,%ld,%ld,%d,%f,%f,%f,%f\n", BASKET_CONF->COMM_SIZE,num_events,size_events_kb,is_shared, sum_record_time , num_events * BASKET_CONF->COMM_SIZE /
        sum_record_time, sum_playback_time ,
                num_events * BASKET_CONF->COMM_SIZE / sum_playback_time);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(is_shared && BASKET_CONF->MPI_RANK != 0){
        status = chronicle->Close(j_name);
        ASSERT(status, ==, 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(!is_shared || BASKET_CONF->MPI_RANK == 0){
        status = chronicle->Destroy(j_name);
        ASSERT(status, ==, 0);
    }
    free(events);
    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
