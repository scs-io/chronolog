#!/bin/bash
DRIVE=$1
NUM_NODES=$2
SCRIPT_DIR=`pwd`
ZK_CONF=${SCRIPT_DIR}/conf/config/zoo_${NUM_NODES}_${DRIVE}.conf
NODES_FILE=${SCRIPT_DIR}/conf/serverlists/${DRIVE}_${NUM_NODES}
NODES=$(cat ${NODES_FILE})
ZK_SDIR=$(sed -n "2p" /home/hdevarajan/projects/chronolog/test/bookkeeper/scripts/conf/config/zoo_8_nvme.conf | tr "=" " " | awk -F" " '{print $2}')
for node in $NODES
do
ssh ${node} "kill -9 \$(ps -aux | grep zookeeper | awk '{print \$2}')"
done
