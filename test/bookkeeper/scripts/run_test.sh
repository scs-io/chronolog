#!/bin/bash
SCRIPT_DIR=`pwd`
for size in 1024 2048 4096 4 64
do
for procs in 960
do
echo "${SCRIPT_DIR}/nvme_corfu_start.sh"
echo ""
${SCRIPT_DIR}/nvme_corfu_start.sh
iterations=$((1024*4/size))
size_b=$((size*1024))
echo "mpirun -n ${procs} -f /home/hdevarajan/projects/chronolog/test/corfu/scripts/conf/server_lists/clients java -classpath $(cat ${SCRIPT_DIR}/cp.txt):/home/hdevarajan/projects/chronolog/test/corfu/target/corfu-test-1.0-SNAPSHOT.jar org.chronolog.test.corfu.CorfuTest ares-comp-01:9000,ares-comp-02:9000,ares-comp-03:9000,ares-comp-04:9000,ares-comp-05:9000,ares-comp-06:9000,ares-comp-07:9000,ares-comp-08:9000 ${iterations} ${size_b}  >> /home/hdevarajan/projects/chronolog/test/corfu/scripts/run_${procs}_${size_b}.log"
echo ""
echo ""
echo ""
mpirun -n ${procs} -f /home/hdevarajan/projects/chronolog/test/corfu/scripts/conf/server_lists/clients java -classpath $(cat ${SCRIPT_DIR}/cp.txt):/home/hdevarajan/projects/chronolog/test/bookkeeper/target/bookeeper-test-1.0-SNAPSHOT.jar org.chronolog.test.bookeeper.BookeeperTest ares-comp-25:2181,ares-comp-26:2181,ares-comp-27:2181,ares-comp-28:2181,ares-comp-29:2181,ares-comp-30:2181,ares-comp-31:2181,ares-comp-32:2181 ${iterations} ${size_b}  >> /home/hdevarajan/projects/chronolog/test/corfu/scripts/run_${procs}_${size_b}.log
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
done
done
