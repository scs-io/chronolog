#!/bin/bash
DRIVE=$1
NUM_NODES=$2
SCRIPT_DIR=`pwd`
NODES_FILE=${SCRIPT_DIR}/conf/serverlists/${DRIVE}_${NUM_NODES}
NODES=$(cat ${NODES_FILE})
${BK_DIR}/bin/bookkeeper shell metaformat -force
for node in $NODES
do
echo "Configuring node $node"
ssh ${node} "mkdir -p /mnt/${DRIVE}/hdevarajan/bk/txn"
ssh ${node} "mkdir -p /mnt/${DRIVE}/hdevarajan/bk/bk-data"
ssh ${node} "${BK_DIR}/bin/bookkeeper shell bookieformat -nonInteractive -force"
ssh ${node} "nohup ${BK_DIR}/bin/bookkeeper bookie > /dev/null 2>&1 &"
sleep 3
COUNT=$((COUNT+1))
done
