#!/bin/bash
DRIVE=$1
NUM_NODES=$2
SCRIPT_DIR=`pwd`
NODES_FILE=${SCRIPT_DIR}/conf/serverlists/${DRIVE}_${NUM_NODES}
NODES=$(cat ${NODES_FILE})
for node in $NODES
do
ssh ${node} "kill -9 \$(ps -aux | grep bookkeeper | awk '{print \$2}')"
done
