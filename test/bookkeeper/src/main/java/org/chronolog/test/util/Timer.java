package org.chronolog.test.util;

public class Timer {
    private long t1;
    private double elapsed_time;
    public Timer(){
        elapsed_time=0;
    }
    public void resumeTime() {
        t1 = System.currentTimeMillis();
    }
    public double pauseTime() {
        long t2 = System.currentTimeMillis();
        elapsed_time += t2 - t1;
        return elapsed_time/1000.0;
    }
    public double getElapsedTime() {
        return elapsed_time/1000.0;
    }
}
