package org.chronolog.test.bookeeper;

import org.apache.bookkeeper.client.BKException;
import org.apache.bookkeeper.client.BookKeeper;
import org.apache.bookkeeper.client.LedgerEntry;
import org.apache.bookkeeper.client.LedgerHandle;
import org.apache.bookkeeper.conf.ClientConfiguration;
import org.chronolog.test.util.Timer;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

public class BookeeperTest {
    public static void main(String args[]) throws InterruptedException, BKException, IOException {
        String zkConnectionString=args[0];
        int create=1;
        long numEvents=1000,sizeOfEvent=1024,id=0;
        if(args.length > 1) numEvents = Long.parseLong(args[1]);
        if(args.length > 2) sizeOfEvent = Long.parseLong(args[2]);
        if(args.length > 3) create = Integer.parseInt(args[3]);
        if(args.length > 4) id = Long.parseLong(args[4]);
        ClientConfiguration config = new ClientConfiguration();
        config.setZkServers(zkConnectionString);
        config.setAddEntryTimeout(2000);
        BookKeeper bkClient = new BookKeeper(config);
        byte[] password = "some-password".getBytes();
        LedgerHandle handle;
        if(create == 1){
            handle = bkClient.createLedger(BookKeeper.DigestType.MAC, password);
            System.out.println("LedgerId:"+handle.getId());
        }else{
            handle = bkClient.openLedger(id,BookKeeper.DigestType.MAC, password);
        }

        byte[] array = new byte[(int) sizeOfEvent]; // length is bounded by 7
        new Random().nextBytes(array);
        String data = new String(array, Charset.forName("UTF-8"));
        Timer appendTimer=new Timer();
        Timer tailTimer=new Timer();
        long i=0;
        try {
            for(i=0;i<numEvents;++i){
                appendTimer.resumeTime();
                long entryId = handle.addEntry(data.getBytes());
                appendTimer.pauseTime();
                tailTimer.resumeTime();
                LedgerEntry entry = handle.readLastEntry();
                tailTimer.pauseTime();
                String read_data=new String(entry.getEntry());
                assert (read_data.equals(data));
            }
        }catch(Exception e){

        }
        System.out.println("Events,"+i+",Append,"+appendTimer.getElapsedTime()+",Tail,"+tailTimer.getElapsedTime());
        handle.close();
    }
}
