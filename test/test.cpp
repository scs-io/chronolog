//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    /*if(BASKET_CONF->MPI_RANK==0){
        printf("%d ready for attach\n", BASKET_CONF->COMM_SIZE);
        fflush(stdout);
        getchar();
    }
    MPI_Barrier(MPI_COMM_WORLD);*/
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    CHRONOLOG_CONF->ConfigureChronologClient();
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();
    char journal_name[20];
    snprintf(journal_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(journal_name);
    int status = chronicle->Create(j_name);
    assert(status == 0);
    Event e1,e2,e3;
    e1.journal_name_=CharStruct(journal_name);
    e1.event_id=1+BASKET_CONF->MPI_RANK*10;
    e2.journal_name_=CharStruct(journal_name);
    e2.event_id=2+BASKET_CONF->MPI_RANK*10;
    e3.journal_name_=CharStruct(journal_name);
    e3.event_id=3+BASKET_CONF->MPI_RANK*10;
    std::string data="Hello World";
    status = chronicle->Append(e1, data);
    assert(status == 0);
    status = chronicle->Append(e2,data);
    assert(status == 0);
    status = chronicle->Append(e3,data);
    assert(status == 0);
    Event tail;
    std::string tail_data;
    tail.journal_name_=CharStruct(journal_name);
    status = chronicle->Tail(tail,tail_data);
    assert(status == 0);
    assert(tail == e3);
    assert(tail_data == data);
    std::string ret_data;
    status = chronicle->Retrieve(e2,ret_data);
    assert(status == 0);
    assert(ret_data == data);
    auto events = std::vector<std::pair<Event, std::string>>();
    status = chronicle->RetrieveRange(e2.journal_name_,e2.event_id,e3.event_id,events);
    assert(status == 0);
    assert(events.size() == 2);
    for(auto result:events){
        assert(result.second==data);
    }
    status = chronicle->Destroy(j_name);
    assert(status == 0);
    auto metadata = basket::Singleton<chronolog::Metadata>::GetInstance();
    Location location;
    location.path="test.bat";
    location.offset=0;
    location.event_size=1;
    status = metadata->Put(e1,location);
    assert(status == 0);
    status = metadata->Put(e2,location);
    assert(status == 0);
    auto es = std::vector<std::pair<Event,Location>>();
    status = metadata->GetRange(e1,e2,es);
    assert(status == 0);
    assert(es.size() == 2);
    for(auto result:es){
        assert(result.second==location);
    }
    Location location2;
    status = metadata->Get(e1,location2);
    assert(status == 0);
    assert(location == location2);
    status = metadata->Delete(e1);
    assert(status == 0);
    status = metadata->Get(e1,location2);
    assert(status == -1);
    MPI_Finalize();
    return 0;
}