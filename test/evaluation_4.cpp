//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    CHRONOLOG_CONF->ConfigureChronologClient();
    /**
     * Flushing
     * test case
     * NVM journal nodes: 1,2,4
     * flusher nodes:1,2,4
     * Workload on Client: 4 chronicles spread across all processes different event rates (emulated with sleeps)
     *  - Everyone writes to their chronicle
     * Workload to time: Just read from NVMe and write to SSD
     * Calculate time to finish all chronicles
     */

    //Define the Chronicle
    CHRONOLOG_CONF->ConfigureChronologClient();
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();

    //Create the Chronicles inside the distributed journal.
    char journal_name[20];
    //Give every chronicle a different name based on MPI rank. So every thread talks to its own chronicle
    snprintf(journal_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    auto j_name=CharStruct(journal_name);
    int status = chronicle->Create(j_name);
    //Ensure everyone has created it correctly
    assert(status == 0);
    MPI_Barrier(MPI_COMM_WORLD);

    //Read the inputs.
    int num_events;
    size_t size_events;
    int sleep_mult;
    int random;
    if(argc==3){
        num_events=(int)std::strtol(argv[1], nullptr, 0);
        size_events=std::strtol(argv[2], nullptr, 0);
        sleep_mult = 1;
        random = 0;
    }
    else if(argc==5){
        num_events=(int)std::strtol(argv[1], nullptr, 0);
        size_events=std::strtol(argv[2], nullptr, 0);
        sleep_mult=(int)std::strtol(argv[3], nullptr, 0);;
        random=(int)std::strtol(argv[4], nullptr, 0);;
    }
    else{
        num_events=1000;
        size_events=4096;
        sleep_mult = 1;
        random = 0;
    }

    //Generate the events.
    auto* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(journal_name);
        events[i].event_id = GetTimeStamp();
        //Make them sleep so that timestamps are different.
        usleep(BASKET_CONF->MPI_RANK);
    }

    std::string data= random_string(size_events);
    for(int i=0;i<num_events;++i){
        status = chronicle->Append(events[i], data);
        assert(status == 0);
        if(random == 1){
            usleep(rand() % (BASKET_CONF->MPI_RANK * sleep_mult));
        }
        else{
            usleep(BASKET_CONF->MPI_RANK * sleep_mult);
        }
    }
    //We dont really want to retrieve data, that is flinks job
//    for(int i=0;i<num_events;++i){
//        std::string return_data;
//        status = chronicle->Retrieve(events[i], return_data);
//        assert(status == 0);
//        assert(return_data==data);
//    }
    MPI_Barrier(MPI_COMM_WORLD);

    //Lets wait for the Chronicle to be empty
    Event e = Event();
    std::string tail_data;
    int tail_status = 0;
    while(tail_status == 0){
        tail_status = chronicle->Tail(e, tail_data);
        sleep(5);
    }

    //Once all of them have finish, destroy and exit
    MPI_Barrier(MPI_COMM_WORLD);
    status = chronicle->Destroy(j_name);
    assert(status == 0);
    MPI_Finalize();

    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}