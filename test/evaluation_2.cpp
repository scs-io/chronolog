//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    if (BASKET_CONF->COMM_SIZE != 1) {
        printf("Test requires 1 client process\n");
    }

    CHRONOLOG_CONF->ConfigureChronologClient();

    const int num_tests = 3;
    const int num_events[num_tests] = {10000, 100000, 1000000};
    const size_t size_events = 1000;

    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();

    Event *events;
    char journal_name[20];
    CharStruct j_name;
    int status;

    for (int test_num = 0; test_num < num_tests; test_num++) {
        snprintf(journal_name, 20, "test_%d", test_num);
        j_name=CharStruct(journal_name);

        // Create journal
        BASKET_CONF->IS_SERVER = false;
        status = chronicle->Create(j_name);
        assert(status == 0);

        events = static_cast<Event *>(malloc(sizeof(Event) * num_events[test_num]));
        for(int i=0;i<num_events[test_num];++i){
            events[i]=Event();
            events[i].journal_name_= j_name;
            // events[i].event_id = i;
            events[i].event_id = GetTimeStamp();
            usleep(BASKET_CONF->MPI_RANK);
        }
        std::string data= random_string(size_events);

        for (int i = 0; i < num_events[test_num]; i++) {
            // printf("append %d\n", i);
            status = chronicle->Append(events[i], data);
            assert(status == 0);
        }


        // Range queries of 100 elements each
        const int step_size = 100;
        auto range_events = std::vector<std::pair<Event, std::string>>();
        std::string ret_data;

        Timer test_timer=Timer();
        for (int i = 0; i < num_events[test_num]; i+=step_size) {
            // printf("retrieve range %d to %d\n", events[i].event_id, events[i+step_size-1].event_id);
            test_timer.resumeTime();
            status = chronicle->RetrieveRange(events[i].journal_name_, events[i].event_id, events[i+step_size-1].event_id, range_events);
            test_timer.pauseTime();
            assert(status == 0);
        }

        double test_time = test_timer.getElapsedTime();
        double test_throughput = num_events[test_num]/test_time*1000*size_events/1024/1024;
        double test_throughput_eps = num_events[test_num]/test_time*1000;
        printf("Test %d throughput: %f events per second\n", test_num, test_throughput_eps);

        status = chronicle->Destroy(j_name);
        assert(status == 0);
        free(events);
    }
    
    /**
     * Performance of Indexer (# of events then do range queries)
     *
     * test case
     * Single process test of putting 10k, 100K and 1M put request
     * - Put all the data first
     * - barrier
     * - do range queries (of around 100 keys)
     *
     * Calculate the throughput of the range queries
     */


    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
