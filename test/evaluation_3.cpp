//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int random_list(int count, ...) {
    int result = 0;
    int selection_index = random() % count;
    va_list args;
    va_start(args, count);
    for (int i = 0; i < count; ++i) {
        if (i == selection_index) {
            result = va_arg(args, int);
            break;
        }
        va_arg(args, int);
    }
    va_end(args);
    return result;
}

int main(int argc, char* argv[]){
    int num_clients = 1;
    if (argc == 2) {
        num_clients = atoi(argv[1]);  // for num_clients of 1, 2, 4, 8
    }

    MPI_Init(&argc,&argv);

    CHRONOLOG_CONF->ConfigureChronologClient();
    BASKET_CONF->RPC_THREADS = 4;
    const int num_tests = 3;
    const int size_events[num_tests] = {4000, 1000000, random_list(5, 16, 4000, 64000, 1000000, 4000000)};
    const int total_size_events = 16000000000;
    int num_events[num_tests];
    for (int i = 0; i < num_tests; i++) {
        num_events[i] = total_size_events / size_events[i];
    }

    int status;
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();

    for (int test_num = 0; test_num < num_tests; test_num++) {
        // initialize events
        Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events[test_num]));
        char journal_name[20];
        snprintf(journal_name, 20, "test");
        for(int i=0;i<num_events[test_num];++i){
            events[i]=Event();
            events[i].journal_name_= CharStruct(journal_name);
            events[i].event_id = GetTimeStamp();
            usleep(BASKET_CONF->MPI_RANK);
        }
        std::string data= random_string(size_events[test_num]);

        // create journals
        snprintf(journal_name, 20, "test_%d", BASKET_CONF->MPI_RANK);
        auto j_name = CharStruct(journal_name);

        status = chronicle->Create(j_name);
        assert(status == 0);

        MPI_Barrier(MPI_COMM_WORLD);

        
        // write and tail read data
        Event tail;
        std::string tail_data;

        Timer write_timer=Timer();
        Timer tail_timer=Timer();
        for (int i = 0; i < num_events[test_num]; i++) {
            // append each data
            write_timer.resumeTime();
            status = chronicle->Append(events[i], data);
            write_timer.pauseTime();
            assert(status == 0);
            
            // tail read each data
            tail_timer.resumeTime();
            status = chronicle->Tail(tail,tail_data);
            tail_timer.pauseTime();
            assert(status == 0);
            assert(tail == events[i]);
            assert(tail_data == data);
        }

        printf("Time to append events for test size of %d bytes per event: %f\n", size_events[test_num], write_timer.getElapsedTime());
        printf("Time to get tail for test size of %d bytes per event: %f\n", size_events[test_num], tail_timer.getElapsedTime());


        MPI_Barrier(MPI_COMM_WORLD);

        // read all data
        Timer read_timer=Timer();
        std::string ret_data;
        for (int i = 0; i < num_events[test_num]; i++) {
            // read each data
            read_timer.resumeTime();
            status = chronicle->Retrieve(events[i],ret_data);
            read_timer.pauseTime();
            assert(status == 0);
            assert(ret_data == data);
        }

        printf("Time to read events for test size of %d bytes per event: %f\n", size_events[test_num], read_timer.getElapsedTime());

        // range read all data
        Timer range_read_timer=Timer();
        auto ret_events = std::vector<std::pair<Event, std::string>>();
        const int read_range = 100;
        for (int i = 0; i < num_events[test_num]; i+=read_range) {
            // range read data
            range_read_timer.resumeTime();
            status = chronicle->RetrieveRange(events[i].journal_name_,
                                              events[i].event_id,
                                              events[i+read_range-1].event_id,
                                              ret_events);
            range_read_timer.pauseTime();
            assert(status == 0);
            assert(ret_events.size() == read_range);
        }

        printf("Time to range read events with read range %d for test size of %d bytes per event: %f\n", read_range, size_events[test_num], range_read_timer.getElapsedTime());

        // clean up before next test
        free(events);
    }
    /**
     * Performance of NVMe Journal
     * Comparision:Bookkeeper VS Corfu/Zlog
     * test case
     * 4 servers of NVM journal(4 threads * 10 processes per node)
     * Client nodes: 1,2,4,8
     * total I/O of 16GB
     * three message sizes: 4KB, 1MB and Random (16B, 4KB, 64KB, 1MB, 4MB)
     * Journal per process
     *  - first write and tail read each data
     *  - synchronize
     *  - read all data
     *  - Range read all data
     * Time each operation (i.e., write, tail read, read and read range) separately
     */


    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
