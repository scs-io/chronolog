//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    CHRONOLOG_CONF->ConfigureChronologClient();
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();
    char journal_name[20];
    snprintf(journal_name, 20, "test");
    CharStruct j_name=CharStruct(journal_name);
    int status;
    if(BASKET_CONF->MPI_RANK ==0){
        status = chronicle->Create(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK !=0){
        status = chronicle->Open(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    int num_events;
    size_t size_events;
    if(argc==3){
        num_events=(int)std::strtol(argv[1], nullptr, 0);
        size_events=std::strtol(argv[2], nullptr, 0);
    }
    else{
        num_events=1000;
        size_events=4096;
    }
    Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(journal_name);
        events[i].event_id = GetTimeStamp();
        usleep(BASKET_CONF->MPI_RANK);
    }
    std::string data= random_string(size_events);
    for(int i=0;i<num_events;++i){
        status = chronicle->Append(events[i], data);
        assert(status == 0);
    }
    for(int i=0;i<num_events;++i){
        std::string return_data;
        status = chronicle->Retrieve(events[i], return_data);
        assert(status == 0);
        assert(return_data==data);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK !=0){
        status = chronicle->Close(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK ==0){
        status = chronicle->Destroy(j_name);
        assert(status == 0);
    }
    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}