//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <c++/chronolog/client/client.h>
#include <chrono>

static auto start = std::chrono::high_resolution_clock::now();
static uint32_t GetTimeStamp(){
    auto finish = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count();
}
std::string random_string( size_t length );

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);

    // if (BASKET_CONF->COMM_SIZE != 160) {
    //     printf("Test requires 160 processes\n");
    // }
    int num_events = 1000;
    size_t size_events_kb = 1000;
    bool is_shared = false;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 10);
    if(argc > 3) size_events_kb=std::strtol(argv[3], nullptr, 10);
    if(argc > 4) is_shared = std::strtol(argv[4], nullptr, 10);
    CHRONOLOG_CONF->ConfigureChronologClient();
    auto chronicle = basket::Singleton<chronolog::Chronicle>::GetInstance();
    char chronicle_name[20];
    if(is_shared) snprintf(chronicle_name, 20, "test");
    else snprintf(chronicle_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(chronicle_name);
    int status;
    // Shared journal test
    if(!is_shared || BASKET_CONF->MPI_RANK ==0){
        status = chronicle->Create(j_name);
        assert(status == 0);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(is_shared && BASKET_CONF->MPI_RANK !=0){
        status = chronicle->Open(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(chronicle_name);
        events[i].event_id = GetTimeStamp();
        usleep(BASKET_CONF->MPI_RANK);
    }
    std::string data= random_string(size_events_kb*1024);



    for (int i = 0; i < num_events; i++) {
        status = chronicle->Append(events[i], data);
        assert(status == 0);
        Event e;
        e.journal_name_ = events[i].journal_name_;
        std::string tail_data;
        status = chronicle->Tail(e, tail_data);
    }
    Timer replay_timer=Timer();
    uint32_t piece_size = 1024*1024;
    for(uint32_t i=0;i<=num_events;++i){
        usleep(1000);
        auto return_range_events=std::vector<std::pair<Event, std::string>>();
        replay_timer.resumeTime();
        status = chronicle->RetrieveRangeChronoplayer(events[i].journal_name_,events[i].event_id,events[i].event_id+piece_size,return_range_events);
        replay_timer.pauseTime();
        assert(status == 0);
    }


    double sum_replay_time;
    double local_replay_time = replay_timer.getElapsedTime();
    MPI_Reduce(&local_replay_time, &sum_replay_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (BASKET_CONF->MPI_RANK == 0) {
        sum_replay_time=sum_replay_time/(1000.0*BASKET_CONF->COMM_SIZE);
        printf("#Procs,#Events,SizeEvents(KB),IsShared,Replay Time(s),Replay Throughput(E/s)\n");
        printf("%d,%ld,%ld,%d,%f,%f\n", BASKET_CONF->COMM_SIZE,num_events,size_events_kb,is_shared, sum_replay_time , num_events * BASKET_CONF->COMM_SIZE /
                       sum_replay_time);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if(is_shared && BASKET_CONF->MPI_RANK != 0){
        status = chronicle->Close(j_name);
        assert(status == 0);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(!is_shared || BASKET_CONF->MPI_RANK == 0){
        status = chronicle->Destroy(j_name);
        assert(status == 0);
    }
    free(events);
    MPI_Finalize();
    return 0;
}

std::string random_string( size_t length ){
    auto randchar = []() -> char
    {
        const char charset[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
