#!/bin/bash
SCRIPT_DIR=`pwd`
rm ${SCRIPT_DIR}/corfu_nvme.log
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/nvme)
PORT=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['PORT']")
LOG_PATH=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['NVME_LOG_PATH']")
CACHE_HEAP_RATIO=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['CACHE_HEAP_RATIO']")
rm ${SCRIPT_DIR}/corfu_nvme.lock
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
ssh ${node_name} "kill -9 \$(ps -aux | grep corfu | awk '{print \$2}')"
ssh ${node_name} /bin/bash << EOF
rm -rf ${LOG_PATH}
mkdir -p ${LOG_PATH}
EOF
done


for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
ssh ${node_name} /bin/bash << EOF
nohup ${CORFU_DIR}/bin/corfu_server -a ${node_name} -l ${LOG_PATH} -T 1 -c ${CACHE_HEAP_RATIO} -C None ${PORT} >> ${SCRIPT_DIR}/corfu_nvme.log 2>&1 &
CORFU_PID=\$!
echo $CORFU_PID >> ${SCRIPT_DIR}/corfu_nvme.lock

EOF
done

${CORFU_DIR}/bin/corfu_bootstrap_cluster -l ${SCRIPT_DIR}/conf/config/nvme_layout.json
