#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/hdd)
PORT=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['PORT']")
LOG_PATH=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['HDD_LOG_PATH']")
CACHE_HEAP_RATIO=$(cat ${SCRIPT_DIR}/conf/config/corfu.json | python -c "import sys, json; print json.load(sys.stdin)['CACHE_HEAP_RATIO']")
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
ssh ${node_name} /bin/bash << EOF
if [ ${node_name} == ${node_procs} ]; then
    ${CORFU_DIR}/bin/corfu_server -a ${node_name} -m ${PORT} -l {LOG_PATH} -T 1 -c ${CACHE_HEAP_RATIO} &
else
    ${CORFU_DIR}/bin/corfu_server -a ${node_name} -m ${PORT} -l {LOG_PATH} -T ${node_procs} -c ${CACHE_HEAP_RATIO} &
fi
EOF
done

${CORFU_DIR}/bin/corfu_bootstrap_cluster -l ${SCRIPT_DIR}/conf/config/hdd_layout.json