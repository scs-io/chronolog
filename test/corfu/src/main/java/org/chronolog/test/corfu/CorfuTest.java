package org.chronolog.test.corfu;

import org.chronolog.test.util.Timer;
import org.corfudb.runtime.CorfuRuntime;
import org.corfudb.runtime.collections.SMRMap;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.Random;

public class CorfuTest {
    public static void main(String args[]){
        String connectionString=args[0];
        String[] connections=connectionString.split(",");
        int num=(new Random()).nextInt(connections.length);
        String stream="A";
        long numEvents=1000,sizeOfEvent=1024;
        if(args.length > 1) numEvents = Long.parseLong(args[1]);
        if(args.length > 2) sizeOfEvent = Long.parseLong(args[2]);
        if(args.length > 3) stream = args[3];
        CorfuRuntime rt = new CorfuRuntime(connections[num]).connect();
        Map<Long,String> map = (Map<Long,String>)rt.getObjectsView()
                .build()
                .setStreamName(stream)
                .setType(SMRMap.class)
                .open();
        byte[] array = new byte[(int) sizeOfEvent]; // length is bounded by 7
        new Random().nextBytes(array);
        String data = new String(array, Charset.forName("UTF-8"));
        Timer appendTimer=new Timer();
        Timer tailTimer=new Timer();
        long i=0;
        try {
        for(i=0;i<numEvents;++i){

                appendTimer.resumeTime();
                map.put(i, data);
                appendTimer.pauseTime();
                tailTimer.resumeTime();
                String previous = map.get(i);
                tailTimer.pauseTime();
                assert (previous.equals(data));

        }
        }catch(Exception e){

        }
        System.out.println("Events,"+i+",Append,"+appendTimer.getElapsedTime()+",Tail,"+tailTimer.getElapsedTime());
    }
}
