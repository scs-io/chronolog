//
// Created by jaime on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/jchronolog/pkv/jchronolog_pkv_PKV.h>
#include <chronolog/common/data_structure.h>
#include <c++/pkv/client/client.h>
#include <c++/jchronolog/util/util.h>
#include <chronolog/common/configuration_manager.h>

//native void c_pkv_init(String name, int my_server_,int num_servers_);
void Java_jchronolog_pkv_PKV_c_1pkv_1init(JNIEnv *env, jobject, jstring conf) {
    intialize();
    CHRONOLOG_CONF->CONFIGURATION_FILE = CharStruct(string_j2c(env, conf));
    CHRONOLOG_CONF->LoadConfiguration();
}

void Java_jchronolog_pkv_PKV_c_1pkv_1exit(JNIEnv * env, jobject) {
   finalize();
}

void Java_jchronolog_pkv_PKV_c_1pkv_1init_1client(JNIEnv * env, jobject, jstring name) {
    CHRONOLOG_CONF->ConfigurePKVClient();
    std::string c_name = string_j2c(env, name);
    auto client = basket::Singleton<pkv::Client>::GetInstance(c_name);
}

//native Boolean c_pkv_set(Event event, String value);
jboolean Java_jchronolog_pkv_PKV_c_1pkv_1set(JNIEnv *env, jobject, jobject event, jstring value) {
    auto client = basket::Singleton<pkv::Client>::GetInstance("");
    Event e = event_j2c(env, event);
    std::string data = string_j2c(env, value);
    bool status = client->Put(e,data);
    return (jboolean)status;
}

//native Pair<Integer, String> c_pkv_get(Event event);
jobject Java_jchronolog_pkv_PKV_c_1pkv_1get(JNIEnv *env, jobject, jobject event) {
    auto client = basket::Singleton<pkv::Client>::GetInstance("");
    Event e = event_j2c(env, event);
    auto iter = client->Get(e);
    return create_pair(env, iter);
}

//native ArrayList<Pair<Integer, String>> c_pkv_get_range(Event startEvent, Event endEvent);
jobject Java_jchronolog_pkv_PKV_c_1pkv_1get_1range(JNIEnv *env, jobject, jobject event_start, jobject event_end) {
    auto client = basket::Singleton<pkv::Client>::GetInstance("");
    Event e_start = event_j2c(env, event_start);
    Event e_end = event_j2c(env, event_end);
    auto iter = client->GetRange(e_start, e_end);
    //We could avoid the next two lines bringing an empty array through function.
    jclass clazz = (*env).FindClass("java/util/ArrayList");
    jmethodID constructor_list = (env)->GetMethodID(clazz, "<init>", "()V");
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    jobject list = env->NewObject(clazz, constructor_list);
    for(std::pair<Event,std::string> result : iter){
        jobject pair = create_pair(env, result);
        env->CallBooleanMethod(list, add_list, pair);
    }
    return list;
}

jboolean Java_jchronolog_pkv_PKV_c_1pkv_1detele(JNIEnv *env, jobject, jobject event){
    auto client = basket::Singleton<pkv::Client>::GetInstance("");
    Event e = event_j2c(env, event);
    bool status = client->Delete(e);
    return (jboolean)status;
}