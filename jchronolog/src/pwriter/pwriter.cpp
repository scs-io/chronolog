
#include <c++/jchronolog/pwriter/jchronolog_pwriter_PWriter.h>
#include <c++/jchronolog/util/util.h>
#include <basket/common/macros.h>
#include <c++/pwriter/pwriter.h>


void Java_jchronolog_pkv_pwriter_PWriter_init(JNIEnv *, jobject){
    intialize();
}

void Java_jchronolog_pwriter_PWriter_exit(JNIEnv *, jobject){
    finalize();
}

void Java_jchronolog_pwriter_PWriter_init_1client(JNIEnv *, jobject){
    basket::Singleton<pwriter::ParallelWriter>::GetInstance();
}

jint Java_jchronolog_pwriter_PWriter_append (JNIEnv * env, jobject, jobject location, jstring data){
    Location l = location_j2c(env,location);
    std::string d = string_j2c(env,data);
    auto client = basket::Singleton<pwriter::ParallelWriter>::GetInstance();
    return (jint) client->Append(l,d);
}

jobject JNICALL Java_jchronolog_pwriter_PWriter_read(JNIEnv *env, jobject, jobject location){
    Location l = location_j2c(env,location);
    std::string d=std::string();
    auto client = basket::Singleton<pwriter::ParallelWriter>::GetInstance();
    int status = client->Read(l,d);
    return create_pair(env, std::pair<int,std::string>(status,d));
}