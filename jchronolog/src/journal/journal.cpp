#include <basket/common/macros.h>
#include <c++/jchronolog/journal/jchronolog_journal_Journal.h>
#include <chronolog/common/data_structure.h>
#include <c++/journal/client/client.h>
#include <c++/jchronolog/util/util.h>
#include <chronolog/common/configuration_manager.h>

void Java_jchronolog_journal_Journal_c_1dj_1init(JNIEnv *env, jobject, jstring conf) {
    intialize();
    CHRONOLOG_CONF->CONFIGURATION_FILE = CharStruct(string_j2c(env, conf));
    CHRONOLOG_CONF->LoadConfiguration();
}

void Java_jchronolog_journal_Journal_c_1dj_1exit(JNIEnv *, jobject) {
    finalize();
}

void Java_jchronolog_journal_Journal_c_1dj_1init_1client(JNIEnv *, jobject) {
    CHRONOLOG_CONF->ConfigureJournalFlinkClient();
    auto client = basket::Singleton<journal::Client>::GetInstance();
}

//private native int c_retrieve(Event event, String data);
//java: new Pair<>(result, data);
jobject Java_jchronolog_journal_Journal_c_1dj_1retrieve(JNIEnv *env, jobject, jobject event) {
    auto client = basket::Singleton<journal::Client>::GetInstance();
    Event e = event_j2c(env, event);
    std::string data;  //TODO: Is this ok
    int status = client->Retrieve(e, data);
    auto return_d=std::pair<int,std::string>(status,data);

    return create_pair(env,return_d);
}

//native int c_dj_range_retrieve(String journal_name, int start_event_id, int end_event_id, List<Pair<Event, String>> events);
//ArrayList<RangePair> events = new ArrayList<>();
//java: new Pair<>(result, events);
jint Java_jchronolog_journal_Journal_c_1dj_1retrieve_1range(JNIEnv *env, jobject, jstring journal_name, jint start_event, jint end_event, jobject list) {
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct name = CharStruct(string_j2c(env, journal_name));
    auto events=vector<std::pair<Event, std::string>>();
    auto start = (uint32_t) start_event;
    auto end = (uint32_t) end_event;
    assert(start >= 0);
    assert(end >= 0);
    int status = client->RetrieveRange(name, start, end, events);
    jclass clazz = (env)->GetObjectClass(list);
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    for(std::pair<Event,std::string> result : events){
        jobject Pair = create_pair(env, result);
        (env)->CallBooleanMethod(list, add_list, Pair);
    }
    return (jint)status;
}

//native int c_dj_Get_Oldest(String journal_name, Event event, int server_index);
//java: new Pair<>(result, event);
jint Java_jchronolog_journal_Journal_c_1dj_1get_1oldest__Ljava_lang_String_2Ljchronolog_utils_Event_2I(JNIEnv *env, jobject, jstring journal_name, jobject event, jint server_index) {
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct name = CharStruct(string_j2c(env, journal_name));
    Event e = event_j2c(env, event); //TODO: it is empty is this the best way?
    int status = client->RetrieveOldestEntry(name, e, (int)server_index);
    if(status == -1) return (jint)status;
    jclass java_event =env->GetObjectClass(event);

    jfieldID journal_name_Id = (env)->GetFieldID(java_event,"journal_name","Ljava/lang/String;");
    (env)->SetObjectField(event, journal_name_Id, (env)->NewStringUTF(e.journal_name_.c_str())); //TODO: Is this necessary. Can i event convert like that?

    jfieldID event_idId = (env)->GetFieldID(java_event,"event_id","I");
    (env)->SetIntField(event, event_idId, e.event_id);

    return (jint)status;
}

//native int c_dj_Get_Oldest(Event event, int server_index);
//java: new Pair<>(result, event);
jobject Java_jchronolog_journal_Journal_c_1dj_1get_1oldest__I(JNIEnv *env, jobject,jint server_index) {
    auto client = basket::Singleton<journal::Client>::GetInstance();
    auto events=std::vector<std::pair<Event,std::string>>();
    static int count = 0;
    try {
        events = client->RetrieveOldestEntry((int)server_index);
        if(count++ == 0) printf("Connected to journal %d.\n",server_index);
    }catch (std::runtime_error er){
        printf("Could not connect to journal %d.\n",server_index);
        count = 0;
    }
    jclass clazz = (*env).FindClass("java/util/ArrayList");
    jmethodID constructor_list = (env)->GetMethodID(clazz, "<init>", "()V");
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    jobject list = env->NewObject(clazz, constructor_list);
    for(std::pair<Event,std::string> result : events){
        jobject pair = create_pair(env, result);
        env->CallBooleanMethod(list, add_list, pair);
    }
    return list;
}

jint Java_jchronolog_journal_Journal_c_1dj_1create(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct journal_name = CharStruct(string_j2c(env, name));
    int status = client->Create(journal_name);
    return (jint)status;
}

jint Java_jchronolog_journal_Journal_c_1dj_1destroy(JNIEnv *env, jobject, jstring name) {
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct journal_name = CharStruct(string_j2c(env, name));
    int status = client->Destroy(journal_name);
    return (jint)status;
}

jint Java_jchronolog_journal_Journal_c_1dj_1open(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct journal_name = CharStruct(string_j2c(env, name));
    int status = client->Open(journal_name);
    return (jint)status;
}

jint Java_jchronolog_journal_Journal_c_1dj_1close(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct journal_name = CharStruct(string_j2c(env, name));
    int status = client->Close(journal_name);
    return (jint)status;
}

jint Java_jchronolog_journal_Journal_c_1dj_1append(JNIEnv *env, jobject, jobject event, jstring data){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    Event e = event_j2c(env, event);
    std::string c_data = string_j2c(env, data);
    int status = client->Append(e, c_data);
    return (jint)status;
}

jobject Java_jchronolog_journal_Journal_c_1dj_1tail(JNIEnv *env, jobject, jobject event){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    Event e = event_j2c(env, event);
    std::string data = "";
    int status = client->Tail(e, data);

    jclass java_event =env->GetObjectClass(event);

    jfieldID journal_name_Id = (env)->GetFieldID(java_event,"journal_name","Ljava/lang/String;");
    (env)->SetObjectField(event, journal_name_Id, (env)->NewStringUTF(e.journal_name_.c_str())); //TODO: Is this necessary. Can i event convert like that?

    jfieldID event_idId = (env)->GetFieldID(java_event,"event_id","I");
    (env)->SetIntField(event, event_idId, e.event_id);
    auto return_pair= std::pair<int,std::string>(status,data);
    return create_pair(env,return_pair);
}

jint Java_jchronolog_journal_Journal_c_1dj_1delete_1entry(JNIEnv *env, jobject, jobject event){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    Event e = event_j2c(env, event);
    int status = client->DeleteEntry(e);
    return (jint)status;
}

jint Java_jchronolog_journal_Journal_c_1dj_1check_1status(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<journal::Client>::GetInstance();
    CharStruct journal_name = CharStruct(string_j2c(env, name));
    int status = client->CheckJournal(journal_name);
    return (jint)status;
}

