package jchronolog.pwriter;

import jchronolog.utils.Location;
import jchronolog.utils.Pair;

import java.util.concurrent.Semaphore;

public class PWriter {
    static private boolean is_initialized;
    private static Semaphore lock = new Semaphore(1);
    static {
        System.loadLibrary("jchronolog");
    }

    public void Init() throws InterruptedException{
        lock.acquire();
        init();
        lock.release();
    }

    public void Exit() throws InterruptedException{
        lock.acquire();
        exit();
        lock.release();
    }

    public void InitClient() throws InterruptedException{
        lock.acquire();
        if(!is_initialized){
            init_client();
            is_initialized=true;
        }
        lock.release();
    }
    public int Append(Location location, String data) throws InterruptedException{
        lock.acquire();
        int status = append(location,data);
        lock.release();
        return status;
    }
    public Pair<Integer,String> Read(Location location) throws InterruptedException{
        lock.acquire();
        Pair<Integer,String> status = read(location);
        lock.release();
        return status;
    }

    private native void init();
    private native void exit();
    private native void init_client();
    private native int append(Location location, String data);
    private native Pair<Integer,String> read(Location location);
}
