
package jchronolog.pkv;

import jchronolog.utils.Event;
import jchronolog.utils.Pair;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;


public class PKV {
    private static Semaphore lock = new Semaphore(1);
    static private boolean is_initialized;

    static {
        System.loadLibrary("jchronolog");
    }

    public void pkvInit(String conf)  throws InterruptedException {
        lock.acquire();
        c_pkv_init(conf);
        lock.release();
    }

    public void pkvClose()  throws InterruptedException {
        lock.acquire();
        c_pkv_exit();
        lock.release();
    }

    public void pkvInitClient(String name)  throws InterruptedException {
        lock.acquire();
        if (!is_initialized) {
            c_pkv_init_client(name);
            is_initialized = true;
        }
        lock.release();
    }


    public boolean pkvSet(Event event, String value)  throws InterruptedException {
        lock.acquire();
        boolean status = c_pkv_set(event, value);
        lock.release();
        return status;
    }

    public Pair<Boolean, String> pkvGet(Event event)  throws InterruptedException {
        lock.acquire();
        Pair<Boolean, String> status = c_pkv_get(event);
        lock.release();
        return status;
    }

    public ArrayList<Pair<Event, String>> pkvGetRange(Event startEvent, Event endEvent)  throws InterruptedException {
        lock.acquire();
        ArrayList<Pair<Event, String>> status = c_pkv_get_range(startEvent, endEvent);
        lock.release();
        return status;
    }

    public boolean pkvDelete(Event event) throws InterruptedException  {
        lock.acquire();
        boolean status = c_pkv_detele(event);
        lock.release();
        return status;
    }

    private native void c_pkv_init(String conf);

    private native void c_pkv_exit();

    private native void c_pkv_init_client(String name);

    private native boolean c_pkv_set(Event event, String value);

    private native Pair<Boolean, String> c_pkv_get(Event event);

    private native ArrayList<Pair<Event, String>> c_pkv_get_range(Event startEvent, Event endEvent);

    private native boolean c_pkv_detele(Event event);
}