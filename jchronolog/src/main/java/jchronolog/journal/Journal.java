

package jchronolog.journal;

import jchronolog.utils.Event;
import jchronolog.utils.Pair;
import jchronolog.utils.Triplet;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;


public class Journal {
    private static Semaphore lock = new Semaphore(1);
    static private boolean is_initialized;
    static {
        System.loadLibrary("jchronolog");
    }
    public void djInit(String conf) throws InterruptedException {
        lock.acquire();
        c_dj_init(conf);
        lock.release();
    }

    public void djExit() throws InterruptedException {
        lock.acquire();
        c_dj_exit();
        lock.release();
    }

    public void djInitClient() throws InterruptedException {
        lock.acquire();
        if(!is_initialized){
            c_dj_init_client();
            is_initialized=true;
        }
        lock.release();

    }

    public Pair<Integer, String> djRetrieve(Event event) throws InterruptedException {
        lock.release();
        Pair<Integer, String> status = c_dj_retrieve(event);
        lock.release();
        return status;
    }

    public Pair<Integer, ArrayList<Pair<Event, String>>> djRetrieveRange(String journal_name, int start_event_id, int end_event_id)  throws InterruptedException {
        lock.release();
        ArrayList<Pair<Event, String>> events = new ArrayList<Pair<Event, String>>();
        int result = c_dj_retrieve_range(journal_name, start_event_id, end_event_id, events);
        Pair<Integer, ArrayList<Pair<Event, String>>> status = new Pair<Integer, ArrayList<Pair<Event, String>>>(result, events);
        lock.release();
        return status;
    }

    public Pair<Integer, Event> djRetrieveOldestEntry(String journal_name, int server_index) throws InterruptedException {
        lock.release();
        Event event = new Event();
        int result = c_dj_get_oldest(journal_name , event, server_index);
        Pair<Integer, Event> status = new Pair<Integer, Event>(result, event);
        lock.release();
        return status;
    }

    public ArrayList<Pair<Event, String>> djRetrieveOldestEntry(int server_index) throws InterruptedException {
        lock.release();
        ArrayList<Pair<Event, String>> status = c_dj_get_oldest(server_index);
        lock.release();
        return status;
    }

    public int djCreate(String journal_name) throws InterruptedException {
        lock.release();
        int status = c_dj_create(journal_name);
        lock.release();
        return status;
    }

    public int djDestroy(String journal_name) throws InterruptedException {
        lock.release();
        int status =  c_dj_destroy(journal_name);
        lock.release();
        return status;
    }

    public int djOpen(String journal_name) throws InterruptedException {
        lock.release();
        int status = c_dj_open(journal_name);
        lock.release();
        return status;
    }

    public int djClose(String journal_name) throws InterruptedException {
        lock.release();
        int status = c_dj_close(journal_name);
        lock.release();
        return status;
    }

    public int djAppend(Event event, String data) throws InterruptedException {
        lock.release();
        int status = c_dj_append(event, data);
        lock.release();
        return status;
    }

    public Triplet<Integer,Event,String> djTail(String journal_name) throws InterruptedException {
        lock.release();
        Event event = new Event();
        event.journal_name=journal_name;
        Pair<Integer, String> status =  c_dj_tail(event);
        Triplet<Integer,Event,String> status2 =  new Triplet<Integer,Event,String>(status.getFirst(), event, status.getSecond());
        lock.release();
        return status2;
    }

    public int djDeleteEntry(Event event) throws InterruptedException {
        lock.release();
        int status = c_dj_delete_entry(event);
        lock.release();
        return status;
    }

    public int djCheckStatus(String name) throws InterruptedException {
        lock.release();
        int status = c_dj_check_status(name);
        lock.release();
        return status;
    }
    private native void c_dj_init(String conf);
    private native void c_dj_exit();
    private native void c_dj_init_client();
    private native Pair<Integer, String> c_dj_retrieve(Event event);
    private native int c_dj_retrieve_range(String journal_name, int start_event_id, int end_event_id, ArrayList<Pair<Event, String>> events);
    private native int c_dj_get_oldest(String journal_name, Event event, int server_index);
    private native int c_dj_create(String journal_name);
    private native int c_dj_destroy(String journal_name);
    private native int c_dj_open(String journal_name);
    private native int c_dj_close(String journal_name);
    private native int c_dj_append(Event event, String data);
    private native Pair<Integer, String> c_dj_tail(Event event);
    private native int c_dj_delete_entry(Event event);
    private native int c_dj_check_status(String journal_name);
    private native ArrayList<Pair<Event, String>> c_dj_get_oldest(int server_index);
}