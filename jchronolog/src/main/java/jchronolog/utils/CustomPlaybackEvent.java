package jchronolog.utils;

public class CustomPlaybackEvent{
    public PlaybackEvent original;
    public Integer start_;
    public Integer end_;
    public Integer len;
    public CustomPlaybackEvent(){
        start_=-2;
        end_=-1;
        len=2;
        original=new PlaybackEvent();
    }
    public CustomPlaybackEvent(PlaybackEvent original, Integer start_, Integer end_,Integer len_){
        this.original=new PlaybackEvent(original);
        this.start_=start_;
        this.end_=end_;
        this.len=len_;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (original.journalName == null ? 0 : original.journalName.hashCode());
        hash = 31 * hash + (this.start_/this.len);
        return hash;
    }
}
