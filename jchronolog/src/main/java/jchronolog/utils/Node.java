package jchronolog.utils;

public class Node {
    public String nodeName;
    public int port;
    public Node(String nodeName, int port) {
        this.nodeName = nodeName;
        this.port = port;
    }
    public Node(Node other) {
        nodeName=other.nodeName;
        port=other.port;
    }
    public Node() {
        nodeName="";
        port=0;
    }
    public String getNodeName(){
        return nodeName;
    }
    public boolean equals(Node other) {
        return nodeName.equals(other.nodeName) && port == other.port;
    }
}
