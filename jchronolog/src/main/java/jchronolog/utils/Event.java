package jchronolog.utils;

public class Event implements Comparable<Event>{
    public String journal_name;
    public int event_id;
    public long initial_timestamp;

    public Event(String journal_name, int event_id) {
        this.journal_name = journal_name;
        this.event_id = event_id;
    }
    public Event() {
        journal_name="";
        event_id=0;
    }
    public String getJournalName(){
        return journal_name;
    }
    public boolean equals(Event other) {
        return journal_name.equals(other.journal_name) && event_id == other.event_id;
    }

    @Override
    public int compareTo(Event o) {
        if(this.equals(o)) return 0;
        else if(this.journal_name.compareTo(o.journal_name)!=0)
            return this.journal_name.compareTo(o.journal_name);
        else{
            if(event_id < o.event_id) return -1;
            else return +1;
        }
    }
}