package jchronolog.utils;

public class PlaybackEvent {
    public String journalName;
    public int startEventId;
    public int endEventId;
    public Node destination;
    public PlaybackEvent(PlaybackEvent other){
        journalName=other.journalName;
        startEventId=other.startEventId;
        endEventId=other.endEventId;
        destination=new Node(other.destination);
    }
    public PlaybackEvent(String journalName, int startEventId,int endEventId,Node destination) {
        this.journalName = journalName;
        this.startEventId = startEventId;
        this.endEventId = endEventId;
        this.destination=new Node(destination);
    }
    public PlaybackEvent() {
        journalName="";
        startEventId=0;
        endEventId=0;
        destination=new Node();
    }
    public boolean equals(PlaybackEvent other) {
        return journalName.equals(other.journalName) &&
                startEventId == other.startEventId &&
                endEventId == other.endEventId &&
                destination == other.destination;
    }
}
