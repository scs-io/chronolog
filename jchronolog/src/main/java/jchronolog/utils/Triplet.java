package jchronolog.utils;

public class Triplet<T1,T2,T3> {
    private T1 data_1;
    private T2 data_2;
    private T3 data_3;

    public Triplet(T1 data_1, T2 data_2, T3 data_3) {
        super();
        this.data_1 = data_1;
        this.data_2 = data_2;
        this.data_3 = data_3;
    }

    public T1 getFirst() {
        return data_1;
    }

    public T2 getSecond() {
        return data_2;
    }

    public T3 getThird() {
        return data_3;
    }

}