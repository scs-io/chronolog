package jchronolog.utils;

public class Location {

    public String path;
    public int offset;
    public int event_size;
    public Location(){
        path="";
        offset=0;
        event_size=0;
    }
    public Location(String path,int offset,int event_size){
        this.path=path;
        this.offset=offset;
        this.event_size=event_size;
    }
    public boolean equals(Location other) {
        return path.equals(other.path) && offset == other.offset && event_size == other.event_size;
    }
}
