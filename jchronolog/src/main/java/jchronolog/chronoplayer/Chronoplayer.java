package jchronolog.chronoplayer;

import jchronolog.utils.*;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Chronoplayer {
    static private boolean chronoplayer_is_initialized;
    private static Semaphore lock = new Semaphore(1);
    static {
        System.loadLibrary("jchronolog");
    }
    public void Init(String conf) throws InterruptedException {
        lock.acquire();
        init(conf);
        lock.release();
    }
    public void Exit() throws InterruptedException {
        lock.acquire();
        exit();
        lock.release();
    }

    public void InitChronoplayer() throws InterruptedException {
        lock.acquire();
        if(!chronoplayer_is_initialized){
            init_chronoplayer();
            chronoplayer_is_initialized=true;
        }
        lock.release();
    }

    public ArrayList<Pair<Event, String>> GetRange(PlaybackEvent event) throws InterruptedException {
        lock.release();
        ArrayList<Pair<Event, String>> status=  get_range(event);
        lock.release();
        return status;
    }
    public ArrayList<PlaybackEvent> GetRangeRequests(int server_index) throws InterruptedException {
        lock.release();
        ArrayList<PlaybackEvent> status= get_range_requests(server_index);
        lock.release();
        return status;
    }
    public int SendData(PlaybackEvent original,ArrayList<Pair<Event, String>> events) throws InterruptedException {
        lock.release();
        int status= send_data(original,events);
        lock.release();
        return status;
    }

    private native void init(String conf);
    private native void exit();
    private native void init_chronoplayer();
    private native ArrayList<Pair<Event, String>> get_range(PlaybackEvent event);
    private native ArrayList<PlaybackEvent> get_range_requests(int server_index);
    private native int send_data(PlaybackEvent original_event,ArrayList<Pair<Event, String>> events);
}

