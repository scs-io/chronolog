package jchronolog.chronolog;

import jchronolog.utils.Event;
import jchronolog.utils.Location;
import jchronolog.utils.Pair;
import jchronolog.utils.PlaybackEvent;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Chronolog {
    static private boolean chronicle_is_initialized,metadata_is_initialized;
    private static Semaphore lock = new Semaphore(1);
    static {
        System.loadLibrary("jchronolog");
    }
    public void Init(String conf) throws InterruptedException {
        lock.acquire();
        init(conf);
        lock.release();
    }
    public void Exit()throws InterruptedException {
        lock.acquire();
        exit();
        lock.release();
    }

    public void InitMetadata() throws InterruptedException{
        lock.acquire();
        if(!metadata_is_initialized){
            init_metadata();
            metadata_is_initialized=true;
        }
        lock.release();
    }

    public int MetadataPut(Event event, Location location)throws InterruptedException{
        lock.release();
        int status=  put_metadata(event,location);
        lock.release();
        return status;
    }

    public int MetadataGet(Event event, Location location)throws InterruptedException{
        lock.release();
        int status=  get_metadata(event,location);
        lock.release();
        return status;
    }
    public int MetadataGetRange(Event start_event, Event end_event, ArrayList<Pair<Event,Location>> events)throws InterruptedException{
        lock.release();
        int status=  get_range_metadata(start_event,end_event,events);
        lock.release();
        return status;
    }
    public int MetadataDelete(Event event)throws InterruptedException{
        lock.release();
        int status=  delete_metadata(event);
        lock.release();
        return status;
    }

    public void InitChronicle() throws InterruptedException {
        lock.acquire();
        if(!chronicle_is_initialized){
            init_chronicle();
            chronicle_is_initialized=true;
        }
        lock.release();
    }

    public int ChronicleCreate(String chronicle_name)throws InterruptedException{
        lock.release();
        int status=  create_chronicle(chronicle_name);
        lock.release();
        return status;
    }
    public int ChronicleDestroy(String chronicle_name)throws InterruptedException{
        lock.release();
        int status= destroy_chronicle(chronicle_name);
        lock.release();
        return status;
    }
    public int ChronicleOpen(String chronicle_name)throws InterruptedException{
        lock.release();
        int status= open_chronicle(chronicle_name);
        lock.release();
        return status;
    }
    public int ChronicleClose(String chronicle_name)throws InterruptedException{
        lock.release();
        int status=  close_chronicle(chronicle_name);
        lock.release();
        return status;
    }
    public int ChronicleAppend(Event event, String data)throws InterruptedException{
        lock.release();
        int status=  append_chronicle(event,data);
        lock.release();
        return status;
    }
    public int ChronicleTail(Event event,String data)throws InterruptedException{
        lock.release();
        int status= tail_chronicle(event,data);
        lock.release();
        return status;
    }
    public int ChronicleRetrieve(Event event, String data)throws InterruptedException{
        lock.release();
        int status= retrieve_chronicle(event,data);
        lock.release();
        return status;
    }
    public ArrayList<Pair<Event,String>> ChronicleRetrieveRange(String chronicle_name, int start_event, int end_event)throws InterruptedException{

        lock.release();
        ArrayList<Pair<Event,String>> status= retrieve_range_chronicle(chronicle_name,start_event,end_event);
        lock.release();
        return status;
    }
    public int ChronicleRetrieveRangeAndSendChronoplayer(PlaybackEvent original_event, String chronicle_name, int start_event, int end_event)throws InterruptedException{

        lock.release();
        int status= retrieve_range_chronicle_and_send(original_event, chronicle_name,start_event,end_event);
        lock.release();
        return status;
    }

    private native void init(String conf);
    private native void exit();
    private native void init_metadata();
    private native int put_metadata(Event event, Location location);
    private native int get_metadata(Event event, Location location);
    private native int get_range_metadata(Event start_event, Event end_event, ArrayList<Pair<Event,Location>> events);
    private native int delete_metadata(Event event);


    private native void init_chronicle();
    private native int create_chronicle(String chronicle_name);
    private native int destroy_chronicle(String chronicle_name);
    private native int open_chronicle(String chronicle_name);
    private native int close_chronicle(String chronicle_name);
    private native int append_chronicle(Event event, String data);
    private native int tail_chronicle(Event event,String data);
    private native int retrieve_chronicle(Event event, String data);
    private native ArrayList<Pair<Event,String>> retrieve_range_chronicle(String chronicle_name, int start_event, int end_event);
    private native int retrieve_range_chronicle_and_send(PlaybackEvent original_event, String chronicle_name, int start_event, int end_event);
}
