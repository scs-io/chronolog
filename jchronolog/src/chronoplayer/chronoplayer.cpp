
#include <c++/jchronolog/chronoplayer/jchronolog_chronoplayer_Chronoplayer.h>
#include <c++/jchronolog/util/util.h>
#include <chronolog/common/configuration_manager.h>
#include "../../../chronoplayer/include/c++/chronoplayer/client/client.h"

void Java_jchronolog_chronoplayer_Chronoplayer_init(JNIEnv * env, jobject, jstring conf) {
    intialize();
    CHRONOLOG_CONF->CONFIGURATION_FILE = CharStruct(string_j2c(env, conf));
    CHRONOLOG_CONF->LoadConfiguration();
}

void Java_jchronolog_chronoplayer_Chronoplayer_exit(JNIEnv *, jobject) {
    finalize();
}

void Java_jchronolog_chronoplayer_Chronoplayer_init_1chronoplayer(JNIEnv *, jobject) {
    CHRONOLOG_CONF->ConfigureChronoplayerFlinkClient();
    auto client = basket::Singleton<chronoplayer::Client>::GetInstance(false);
}

jobject Java_jchronolog_chronoplayer_Chronoplayer_get_1range(JNIEnv * env, jobject, jobject playbackevent_obj) {
    auto client = basket::Singleton<chronoplayer::Client>::GetInstance(false);
    auto events=std::vector<std::pair<Event,std::string>>();
    PlaybackEvent event = playbackevent_j2c(env,playbackevent_obj);
    events = client->GetRange(event);
    jclass clazz = (*env).FindClass("java/util/ArrayList");
    jmethodID constructor_list = (env)->GetMethodID(clazz, "<init>", "()V");
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    jobject list = env->NewObject(clazz, constructor_list);
    for(std::pair<Event,std::string> result : events){
        jobject pair = create_pair(env, result);
        env->CallBooleanMethod(list, add_list, pair);
    }
    return list;
}

jobject Java_jchronolog_chronoplayer_Chronoplayer_get_1range_1requests(JNIEnv * env, jobject, jint server_index) {
    auto client = basket::Singleton<chronoplayer::Client>::GetInstance(false);
    auto events=std::vector<PlaybackEvent>();
    int server_index_ = (int)server_index;
    static int count = 0;
    try {
        events = client->GetRangeRequests(reinterpret_cast<uint16_t &>(server_index_));
        if(count++ == 0) printf("Connected to chronoplayer.\n");
    }catch (std::runtime_error er){
        printf("Could not connect to chronoplayer.\n");
        count = 0;
    }
    jclass clazz = (*env).FindClass("java/util/ArrayList");
    jmethodID constructor_list = (env)->GetMethodID(clazz, "<init>", "()V");
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    jobject list = env->NewObject(clazz, constructor_list);
    for(PlaybackEvent result : events){
        jobject element = playbackevent_c2j(env, result);
        env->CallBooleanMethod(list, add_list, element);
    }
    return list;
}

jint Java_jchronolog_chronoplayer_Chronoplayer_send_1data(JNIEnv *env , jobject, jobject playback, jobject events) {
    auto client = basket::Singleton<chronoplayer::Client>::GetInstance(false);
    PlaybackEvent event_=playbackevent_j2c(env, playback);
    jclass cArrayList = env->FindClass("java/util/ArrayList");
    // retrieve the size and the get method
    jmethodID mSize = env->GetMethodID(cArrayList, "size", "()I");
    jmethodID mGet = env->GetMethodID(cArrayList, "get", "(I)Ljava/lang/Object;");
    jint size = env->CallIntMethod(events, mSize);
    auto events_=std::vector<std::pair<Event,std::string>>();
    for(jint i=0;i<size;i++) {
        jobject pairElement = (jstring)env->CallObjectMethod(events, mGet, i);
        std::pair<Event,std::string> pair_element=create_pair_j2c(env,pairElement);
        events_.push_back(pair_element);
    }
    return client->SendData(event_,events_);
}