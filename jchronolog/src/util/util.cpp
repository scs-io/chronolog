//
// Created by hariharan on 8/16/19.
//
#include <c++/jchronolog/util/util.h>
#include <mpi.h>
#include <basket/common/macros.h>
static boost::mutex mtx;
void intialize() {

    mtx.lock();
    if(!SYSTEM->is_initialized){
        int provided;
        MPI_Init_thread(NULL, NULL,MPI_THREAD_MULTIPLE,&provided);
        if(provided == MPI_THREAD_MULTIPLE){
            printf("max level available is %d\n",provided);
        }
        SYSTEM->is_initialized=true;
    }
    mtx.unlock();
}

void finalize() {
    mtx.lock();
    if(SYSTEM->is_initialized){
        SYSTEM->is_initialized=false;
        MPI_Finalize();
    }
    mtx.unlock();
}

std::string string_j2c(JNIEnv *env, jstring name){
    jboolean isCopy;
    const char *convertedValue = (env)->GetStringUTFChars(name, &isCopy);
    const jsize len = (env)->GetStringLength(name);
    return std::string(convertedValue);
}

Event event_j2c(JNIEnv *env, jobject event){
    Event e;
    jclass java_event =env->GetObjectClass(event);
    jfieldID journal_name_Id = (env)->GetFieldID(java_event,"journal_name","Ljava/lang/String;");
    jobject journal_name_obj = (env)->GetObjectField(event, journal_name_Id);
    jstring journal_name = reinterpret_cast<jstring>(journal_name_obj);
    jfieldID event_idId = (env)->GetFieldID(java_event,"event_id","I");
    jint event_id = (env)->GetIntField(event,event_idId);

    e.journal_name_=CharStruct(string_j2c(env, journal_name).c_str());
    e.event_id=(int)event_id;
    return e;
}

Node node_j2c(JNIEnv *env, jobject node){
    Node e;
    jclass java_event =env->GetObjectClass(node);
    jfieldID node_name_Id = (env)->GetFieldID(java_event, "nodeName", "Ljava/lang/String;");
    jobject node_name_obj = (env)->GetObjectField(node, node_name_Id);
    jstring node_name = reinterpret_cast<jstring>(node_name_obj);
    jfieldID port_id = (env)->GetFieldID(java_event, "port", "I");
    jint port = (env)->GetIntField(node, port_id);

    e.node_name_=CharStruct(string_j2c(env, node_name).c_str());
    e.port_=(int)port;
    return e;
}

PlaybackEvent playbackevent_j2c(JNIEnv *env, jobject event){
    PlaybackEvent e;
    jclass java_event =env->GetObjectClass(event);
    jfieldID journal_name_Id = (env)->GetFieldID(java_event, "journalName", "Ljava/lang/String;");
    jobject journal_name_obj = (env)->GetObjectField(event, journal_name_Id);
    jstring journal_name = reinterpret_cast<jstring>(journal_name_obj);
    jfieldID start_event_id = (env)->GetFieldID(java_event, "startEventId", "I");
    jint start_event = (env)->GetIntField(event, start_event_id);
    jfieldID end_event_id = (env)->GetFieldID(java_event, "endEventId", "I");
    jint end_event = (env)->GetIntField(event, end_event_id);
    jfieldID destination_Id = (env)->GetFieldID(java_event, "destination", "Ljchronolog/utils/Node;");
    jobject destination_obj = (env)->GetObjectField(event, destination_Id);
    e.journal_name_=CharStruct(string_j2c(env, journal_name).c_str());
    e.start_=(int)start_event;
    e.end_=(int)end_event;
    e.destination=node_j2c(env,destination_obj);
    return e;
}

Location location_j2c(JNIEnv *env, jobject location){
    Location l;
    jclass java_event =env->GetObjectClass(location);
    jfieldID path_Id = (env)->GetFieldID(java_event, "path", "Ljava/lang/String;");
    jobject path_obj = (env)->GetObjectField(location, path_Id);
    jstring path = reinterpret_cast<jstring>(path_obj);

    jfieldID offset_id = (env)->GetFieldID(java_event, "offset", "I");
    jint offset = (env)->GetIntField(location, offset_id);

    jfieldID event_size_id = (env)->GetFieldID(java_event, "event_size", "I");
    jint event_size = (env)->GetIntField(location, event_size_id);

    l.path=CharStruct(string_j2c(env, path).c_str());
    l.offset=(int)offset;
    l.event_size=(int)event_size;
    return l;
}

jobject integer_c2j(JNIEnv *env,int value){
    jclass integerClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("java/lang/Integer")));
    jmethodID bool_constructor = (env)->GetMethodID(integerClass, "<init>", "(I)V");
    return (env)->NewObject(integerClass, bool_constructor, value);
}

jobject boolean_c2j(JNIEnv *env,bool value){
    jclass booleanClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("java/lang/Boolean")));
    jmethodID bool_constructor = (env)->GetMethodID(booleanClass, "<init>", "(Z)V");
    return  (env)->NewObject(booleanClass, bool_constructor, (jboolean)value);
}



jstring string_c2j(JNIEnv *env,std::string str){
    return (env)->NewStringUTF(str.c_str());
}

jobject pair_c2j(JNIEnv *env, jobject a, jobject b){
    jclass pairClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("jchronolog/utils/Pair")));
    jmethodID pair_constructor = (env)->GetMethodID(pairClass, "<init>", "(Ljava/lang/Object;Ljava/lang/Object;)V");
    return  (env)->NewObject(pairClass, pair_constructor, a, b);
}

jobject event_c2j(JNIEnv *env,Event event){
    jobject int_obj = integer_c2j(env,event.event_id);
    jstring string_obj = string_c2j(env,event.journal_name_.c_str());
    jclass eventClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("jchronolog/utils/Event")));
    jmethodID event_constructor = (env)->GetMethodID(eventClass, "<init>", "(Ljava/lang/String;I)V");
    jobject event_obj = (env)->NewObject(eventClass, event_constructor, string_obj,event.event_id);
    return event_obj;
}

jobject node_c2j(JNIEnv *env,Node node){
    jobject int_obj = integer_c2j(env, node.port_);
    jstring string_obj = string_c2j(env, node.node_name_.c_str());
    jclass eventClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("jchronolog/utils/Node")));
    jmethodID event_constructor = (env)->GetMethodID(eventClass, "<init>", "(Ljava/lang/String;I)V");
    jobject event_obj = (env)->NewObject(eventClass, event_constructor, string_obj, node.port_);
    return event_obj;
}

jobject playbackevent_c2j(JNIEnv *env,PlaybackEvent event){
    jstring journal_name_obj = string_c2j(env, event.journal_name_.c_str());
    jobject start_event_obj = integer_c2j(env, event.start_);
    jobject end_event_obj = integer_c2j(env, event.end_);
    jobject destination_obj = node_c2j(env,event.destination);
    jclass playbackeventClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("jchronolog/utils/PlaybackEvent")));
    jmethodID playbackevent_constructor = (env)->GetMethodID(playbackeventClass, "<init>", "(Ljava/lang/String;IILjchronolog/utils/Node;)V");
    jobject event_obj = (env)->NewObject(playbackeventClass, playbackevent_constructor, journal_name_obj, event.start_, event.end_,destination_obj);
    return event_obj;
}


jobject location_c2j(JNIEnv *env,Location location){
    jstring string_obj = string_c2j(env, location.path.c_str());
    jclass eventClass = static_cast<jclass>(env->NewGlobalRef(env->FindClass("jchronolog/utils/Location")));
    jmethodID event_constructor = (env)->GetMethodID(eventClass, "<init>", "(Ljava/lang/String;II)V");
    jobject event_obj = (env)->NewObject(eventClass, event_constructor, string_obj,location.offset,location.event_size);
    return event_obj;
}

jobject create_pair(JNIEnv *env, const std::pair<int, std::string>& return_pair){
    jobject int_obj = integer_c2j(env,return_pair.first);
    jstring string_obj = string_c2j(env,return_pair.second);
    jobject pair = pair_c2j(env, int_obj, string_obj);
    return pair;
}

jobject create_pair(JNIEnv *env, const std::pair<bool, std::string>& return_pair){
    jobject boolean_obj = boolean_c2j(env,return_pair.first);
    jstring value =string_c2j(env,return_pair.second);
    jobject pair = pair_c2j(env, boolean_obj, value);
    return pair;
}

jobject create_pair(JNIEnv *env, const std::pair<Event, std::string>& return_pair){
    jobject event_obj = event_c2j(env,return_pair.first);
    jstring data_string_obj = string_c2j(env,return_pair.second);
    jobject pair = pair_c2j(env, event_obj, data_string_obj);
    return pair;
}

std::pair<Event, std::string> create_pair_j2c(JNIEnv *env, jobject pair){
    jclass java_event =env->GetObjectClass(pair);
    jfieldID event_id = (env)->GetFieldID(java_event, "first", "jchronolog/utils/Event");
    jobject event_obj = (env)->GetObjectField(pair, event_id);
    jfieldID data_Id = (env)->GetFieldID(java_event, "second", "Ljava/lang/String;");
    jobject data_obj = (env)->GetObjectField(pair, data_Id);
    jstring data = reinterpret_cast<jstring>(data_obj);
    return std::make_pair<Event, std::string>(event_j2c(env,event_obj),string_j2c(env,data));
}


jobject create_pair(JNIEnv *env, const std::pair<Event, Location> &return_pair) {
    jobject event_obj = event_c2j(env,return_pair.first);
    jobject location_obj = location_c2j(env,return_pair.second);
    jobject pair = pair_c2j(env, event_obj, location_obj);
    return pair;
}


