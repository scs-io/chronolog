package jchronolog.journal;

import jchronolog.utils.Event;
import jchronolog.utils.Pair;
import jchronolog.utils.Triplet;

import java.util.ArrayList;

public class JournalTest {
    public static void main(String args[]) throws InterruptedException{
        Journal client = new Journal();
        String conf;
        if(args.length == 2){
            conf = args[1];
        }
        else{
            System.out.println("Needed conf file");
            return;
        }
        client.djInit(conf);
        client.djInitClient();
        String journal_name = "test_1";
        int status = client.djCreate(journal_name);
        assert(status == 0);
        int MAX_EVENTS=4;
        Event[] events=new Event[MAX_EVENTS];
        for(int i=0;i<MAX_EVENTS;i++){
            events[i]=new Event();
            events[i].journal_name = "";
            events[i].journal_name =journal_name;
            events[i].event_id=i;
        }
        String data = "Hello World";
        for(int i=0;i<MAX_EVENTS-1;++i){
            status = client.djAppend(events[i], data);
            assert(status==0);
        }
        Triplet<Integer,Event,String> ret_data = client.djTail(journal_name);
        assert(ret_data.getFirst() == 0);
        assert(ret_data.getSecond().equals(events[2]));
        assert(ret_data.getThird().equals(data));

        Pair<Integer,String> retrieve = client.djRetrieve(events[1]);
        assert(retrieve.getFirst() == 0);
        assert(retrieve.getSecond().equals(data));
        Pair<Integer, ArrayList<Pair<Event, String>>> vals = client.djRetrieveRange(events[1].journal_name,events[1].event_id,events[2].event_id);
        assert(vals.getFirst() == 0);
        assert(vals.getSecond().size() == 2);
        for(Pair<Event, String> result:vals.getSecond()){
            assert(result.getSecond().equals(data));
        }
        Pair<Integer,Event> retrieveOldestEntry = client.djRetrieveOldestEntry(journal_name,0);
        assert(retrieveOldestEntry.getFirst()==0);
        assert(retrieveOldestEntry.getSecond().equals(events[0]));

        status = client.djDeleteEntry(events[0]);
        assert(status==0);
        status = client.djDeleteEntry(events[0]);
        assert(status==0);
        status = client.djDeleteEntry(events[1]);
        assert(status==0);
        client.djExit();
    }
}
