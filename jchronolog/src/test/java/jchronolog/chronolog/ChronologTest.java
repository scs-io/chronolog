package jchronolog.chronolog;

import jchronolog.utils.Event;
import jchronolog.utils.Location;
import jchronolog.utils.Pair;

import java.util.ArrayList;

public class ChronologTest {
    public static void main(String args[])throws InterruptedException{
        Chronolog client = new Chronolog();
        String conf;
        if(args.length == 2){
            conf = args[1];
        }
        else{
            System.out.println("Needed conf file");
            return;
        }
        client.Init(conf);
        client.InitMetadata();
        String journal_name = "test_1";
        int status = client.ChronicleCreate(journal_name);
        assert(status == 0);
        int MAX_EVENTS=4;
        Event[] events=new Event[MAX_EVENTS];
        for(int i=0;i<MAX_EVENTS;i++){
            events[i]=new Event();
            events[i].journal_name = "";
            events[i].journal_name =journal_name;
            events[i].event_id=i;
        }
        String data = "Hello World";
        for(int i=0;i<MAX_EVENTS-1;++i){
            status = client.ChronicleAppend(events[i], data);
            assert(status==0);
        }
        Event tail=new Event();
        String tail_data="";
        tail.journal_name=journal_name;
        status = client.ChronicleTail(tail,tail_data);
        assert(status == 0);
        assert(tail == events[2]);
        assert(tail_data.equals(data));
        String ret_data="";
        status = client.ChronicleRetrieve(events[1],ret_data);
        assert(status == 0);
        assert(ret_data.equals(data));
        ArrayList<Pair<Event, String>> status2 = client.ChronicleRetrieveRange(events[1].journal_name,events[1].event_id,events[2].event_id);
        assert(status2.size() == 2);
        for(Pair<Event, String> result:status2){
            assert(result.getSecond().equals(data));
        }
        status = client.ChronicleDestroy(journal_name);
        assert(status == 0);

        Location location=new Location();
        location.path="test.bat";
        location.offset=0;
        location.event_size=1;
        status = client.MetadataPut(events[0],location);
        assert(status == 0);
        status = client.MetadataPut(events[1],location);
        assert(status == 0);
        ArrayList<Pair<Event,Location>> es = new ArrayList<Pair<Event,Location>>();
        status = client.MetadataGetRange(events[0],events[1],es);
        assert(status == 0);
        assert(es.size() == 2);
        for(Pair<Event,Location> result:es){
            assert(result.getSecond().equals(location));
        }
        Location location2=new Location();
        status = client.MetadataGet(events[0],location2);
        assert(status == 0);
        assert(location == location2);
        status = client.MetadataDelete(events[0]);
        assert(status == 0);
        status = client.MetadataGet(events[0],location2);
        assert(status == -1);
        client.Exit();
    }
}
