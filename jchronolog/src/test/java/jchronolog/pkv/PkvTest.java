package jchronolog.pkv;

import jchronolog.utils.Event;
import jchronolog.utils.Pair;

import java.util.ArrayList;

public class PkvTest {
    public static void main(String args[])throws InterruptedException{
        PKV pkvClient = new PKV();
        String conf;
        if(args.length == 2){
            conf = args[1];
        }
        else{
            System.out.println("Needed conf file");
            return;
        }
        pkvClient.pkvInit(conf); //Initiate MPI. Should rename to JPKV so that is the constructor
        pkvClient.pkvInitClient("SSD_KV");

        String journal_name = "test_1";
        int MAX_EVENTS=4;
        Event[] events=new Event[MAX_EVENTS];
        for(int i=0;i<MAX_EVENTS;i++){
            events[i]=new Event();
            events[i].journal_name = "";
            events[i].journal_name =journal_name;
            events[i].event_id=i;
        }
        String data = "Hello World";
        for(int i=0;i<MAX_EVENTS-1;++i){
            boolean status = pkvClient.pkvSet(events[i], data);
            assert status : "pkvClient.pkvSet Failed for event_id "+ events[i].event_id;
        }
        for (int i = 0; i < MAX_EVENTS-1; i++) {
            Pair<Boolean,String> pair = pkvClient.pkvGet(events[i]);
            assert(pair.getFirst());
            assert(pair.getSecond().equals(data));
        }
        // Check results on range get
        ArrayList<Pair<Event,String>> results = pkvClient.pkvGetRange(events[1],events[2]);
        assert(results.size() == 2);
        int i=0;
        for(Pair<Event,String> result:results){
            assert(result.getFirst().equals(events[i+1]));
            assert(result.getSecond().equals(data));
            i++;
        }

        // Check results on get of item not put yet
        Pair<Boolean,String> pair = pkvClient.pkvGet(events[MAX_EVENTS-1]);
        assert(!pair.getFirst());

        // Check results on range get including item not put yet on rhs
        ArrayList<Pair<Event,String>> results_ob_right = pkvClient.pkvGetRange(events[1],events[3]);
        assert(results_ob_right.size() == 2);

        for(i = 0; i < results_ob_right.size(); i++){
            assert(results_ob_right.get(i).getFirst().equals(events[i+1]));
            assert(results_ob_right.get(i).getSecond().equals(data));
        }

        // Check results on range get with greater start than end
        ArrayList<Pair<Event,String>> nonsense_results = pkvClient.pkvGetRange(events[2], events[1]);
        assert(nonsense_results.size() == 0);

        // Check status of delete on item
        boolean status = pkvClient.pkvDelete(events[0]);
        assert(status);

        // Check results on range get including deleted item on lhs
        ArrayList<Pair<Event,String>> results_ob_left = pkvClient.pkvGetRange(events[0], events[2]);
        assert(results_ob_left.size() == 2);
        for(i = 0; i < results_ob_left.size(); i++){
            assert(results_ob_left.get(i).getFirst().equals(events[i+1]));
            assert(results_ob_left.get(i).getSecond().equals(data));
        }

        // Check results on range with "hole" in middle
        status = pkvClient.pkvSet(events[0], data);
        assert(status);
        status = pkvClient.pkvDelete(events[1]);
        assert(status);
        ArrayList<Pair<Event,String>> results_hole = pkvClient.pkvGetRange(events[0], events[2]);
        assert(results_hole.size() == 2);
        assert(results_hole.get(0).getFirst().equals(events[0]));
        assert(results_hole.get(0).getSecond().equals(data));
        assert(results_hole.get(1).getFirst().equals(events[2]));
        assert(results_hole.get(1).getSecond().equals(data));

        // Check statuses of more deletes (including after being put a second time)
        status = pkvClient.pkvDelete(events[0]);
        assert(status);
        status = pkvClient.pkvDelete(events[1]);
        assert(status);

        // Check status of delete on item never put
        status = pkvClient.pkvDelete(events[3]);
        assert(!status);
        pkvClient.pkvClose();
    }
}
