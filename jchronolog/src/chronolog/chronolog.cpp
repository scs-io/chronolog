#include <basket/common/macros.h>
#include <c++/jchronolog/chronolog/jchronolog_chronolog_Chronolog.h>
#include <chronolog/common/data_structure.h>
#include <c++/journal/client/client.h>
#include <c++/jchronolog/util/util.h>
#include <chronolog/common/configuration_manager.h>
#include <c++/chronolog/client/client.h>

void Java_jchronolog_chronolog_Chronolog_init(JNIEnv *env, jobject, jstring conf) {
    intialize();
    CHRONOLOG_CONF->CONFIGURATION_FILE = CharStruct(string_j2c(env, conf));
    CHRONOLOG_CONF->LoadConfiguration();
}

void Java_jchronolog_chronolog_Chronolog_exit(JNIEnv *, jobject) {
    finalize();
}

void Java_jchronolog_chronolog_Chronolog_init_1metadata(JNIEnv *, jobject) {
    CHRONOLOG_CONF->ConfigureChronologClient();
    auto client = basket::Singleton<chronolog::Metadata>::GetInstance();
}

jint Java_jchronolog_chronolog_Chronolog_put_1metadata(JNIEnv *env, jobject, jobject event, jobject location) {
    auto client = basket::Singleton<chronolog::Metadata>::GetInstance();
    Event e = event_j2c(env, event);
    Location l = location_j2c(env, location);
    int status = client->Put(e, l);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_get_1metadata(JNIEnv *env, jobject, jobject event, jobject location) {
    auto client = basket::Singleton<chronolog::Metadata>::GetInstance();
    Event e = event_j2c(env, event);
    Location l = location_j2c(env, location);
    return client->Get(e,l);
}

jint Java_jchronolog_chronolog_Chronolog_get_1range_1metadata(JNIEnv *env, jobject, jobject start_event, jobject end_event, jobject events_obj) {
    auto client = basket::Singleton<chronolog::Metadata>::GetInstance();
    Event e_start = event_j2c(env, start_event);
    Event e_end = event_j2c(env, end_event);
    auto events=vector<std::pair<Event, Location>>();
    int status = client->GetRange(e_start, e_end, events);
    jclass clazz = (env)->GetObjectClass(events_obj);
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    for(std::pair<Event,Location> event : events){
        jobject pair = create_pair(env, event);
        (env)->CallBooleanMethod(events_obj, add_list, pair);
    }
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_delete_1metadata
        (JNIEnv *env, jobject, jobject event){
    auto client = basket::Singleton<chronolog::Metadata>::GetInstance();
    Event e = event_j2c(env, event);
    return client->Delete(e);
}

//native int c_dj_Get_Oldest(Event event, int server_index);
//java: new Pair<>(result, event);
void Java_jchronolog_chronolog_Chronolog_init_1chronicle(JNIEnv *env, jobject) {
    CHRONOLOG_CONF->ConfigureChronologClient();
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
}

jint Java_jchronolog_chronolog_Chronolog_create_1chronicle(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct chronicle_name = CharStruct(string_j2c(env, name));
    int status = client->Create(chronicle_name);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_destroy_1chronicle(JNIEnv *env, jobject, jstring name) {
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct chronicle_name = CharStruct(string_j2c(env, name));
    int status = client->Destroy(chronicle_name);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_open_1chronicle(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct chronicle_name = CharStruct(string_j2c(env, name));
    int status = client->Open(chronicle_name);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_close_1chronicle(JNIEnv *env, jobject, jstring name){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct chronicle_name = CharStruct(string_j2c(env, name));
    int status = client->Close(chronicle_name);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_append_1chronicle(JNIEnv *env, jobject, jobject event, jstring data){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    Event e = event_j2c(env, event);
    std::string c_data = string_j2c(env, data);
    int status = client->Append(e, c_data);
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_tail_1chronicle(JNIEnv *env, jobject, jobject event, jstring j_data){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    Event e = event_j2c(env, event);
    std::string data = "";
    int status = client->Tail(e, data);
    jclass java_event =env->GetObjectClass(event);
    jfieldID journal_name_Id = (env)->GetFieldID(java_event,"journal_name","Ljava/lang/String;");
    (env)->SetObjectField(event, journal_name_Id, (env)->NewStringUTF(e.journal_name_.c_str()));
    jfieldID event_idId = (env)->GetFieldID(java_event,"event_id","I");
    (env)->SetIntField(event, event_idId, e.event_id);
    j_data = (env)->NewStringUTF(data.c_str());
    return (jint)status;
}

jint Java_jchronolog_chronolog_Chronolog_retrieve_1chronicle(JNIEnv *env, jobject, jobject event, jstring j_data){
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    Event e = event_j2c(env, event);
    std::string data = "";
    int status = client->Retrieve(e,data);
    j_data = (env)->NewStringUTF(data.c_str());
    return (jint)status;
}

jobject Java_jchronolog_chronolog_Chronolog_retrieve_1range_1chronicle(JNIEnv *env, jobject, jstring journal_name, jint start_event, jint end_event) {
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct name = CharStruct(string_j2c(env, journal_name));
    auto events=vector<std::pair<Event, std::string>>();
    auto start = (uint32_t) start_event;
    auto end = (uint32_t) end_event;
    assert(start >= 0);
    assert(end >= 0);
    int status = client->RetrieveRange(name, start, end, events);
    jclass clazz = (*env).FindClass("java/util/ArrayList");
    jmethodID constructor_list = (env)->GetMethodID(clazz, "<init>", "()V");
    jmethodID add_list = env->GetMethodID(clazz, "add", "(Ljava/lang/Object;)Z");
    jobject list = env->NewObject(clazz, constructor_list);
    for(std::pair<Event,std::string> result : events){
        jobject Pair = create_pair(env, result);
        (env)->CallBooleanMethod(list, add_list, Pair);
    }
    return list;
}

jint Java_jchronolog_chronolog_Chronolog_retrieve_1range_1chronicle_1and_1send(JNIEnv * env, jobject, jobject playback_obj, jstring chronolog_obj, jint start_obj, jint end_obj) {
    auto client = basket::Singleton<chronolog::Chronicle>::GetInstance();
    CharStruct name = CharStruct(string_j2c(env, chronolog_obj));
    auto start = (uint32_t) start_obj;
    auto end = (uint32_t) end_obj;
    PlaybackEvent event_=playbackevent_j2c(env, playback_obj);
    return client->RetrieveRangeAndSendChronoplayer(event_,name,start,end);
}

