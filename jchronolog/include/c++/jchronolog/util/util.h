//
// Created by hariharan on 8/16/19.
//

#ifndef JCHRONOLOG_UTIL_H
#define JCHRONOLOG_UTIL_H

#include <jni.h>
#include <string>
#include <chronolog/common/data_structure.h>
#include <boost/thread/mutex.hpp>

typedef class System{
public:
    System():is_initialized(false){}
    bool is_initialized;
} System;

#define SYSTEM basket::Singleton<System>::GetInstance()

void intialize();
void finalize();

std::string string_j2c(JNIEnv *env, jstring name);

Event event_j2c(JNIEnv *env, jobject event);



Location location_j2c(JNIEnv *env, jobject location);

jobject create_pair(JNIEnv *env, const std::pair<int, std::string>& return_pair);

jobject create_pair(JNIEnv *env, const std::pair<bool, std::string>& return_pair);

jobject create_pair(JNIEnv *env, const std::pair<Event, std::string>& return_pair);

jobject create_pair(JNIEnv *env, const std::pair<Event, Location>& return_pair);

jobject playbackevent_c2j(JNIEnv *env,PlaybackEvent event);

jobject node_c2j(JNIEnv *env,Node node);

PlaybackEvent playbackevent_j2c(JNIEnv *env, jobject event);

Node node_j2c(JNIEnv *env, jobject node);

std::pair<Event, std::string> create_pair_j2c(JNIEnv *env, jobject pair);

#endif //JCHRONOLOG_UTIL_H
