#!/bin/bash
HLOG_DIR=`pwd`

echo "Compiling chronolog"
mkdir build
cd build
if [ "$1" = "clean" ] ; 
then rm -rf ${HLOG_DIR}/build/*
fi
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCMAKE_CXX_FLAGS="${CXXFLAGS} -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux/" ../
if [ "$1" = "clean" ] ;
then make clean
fi
make -j8

echo "Compiling Hlog JNI bindings"
cd ${HLOG_DIR}
cd jchronolog
mvn package
cp ${HLOG_DIR}/jchronolog/target/jchronolog-1.0.jar ${INSTALL_DIR}/lib/

echo "Compiling chronographer"
cd ${HLOG_DIR}
cd chronographer
mvn install
cp ${HLOG_DIR}/chronographer/target/chronographer-1.0.jar ${INSTALL_DIR}/lib/
cd ${HLOG_DIR}
echo "Compilation Complete"

echo "Compiling chronoplayer"
cd ${HLOG_DIR}
cd chronoplayer
mvn install
cp ${HLOG_DIR}/chronoplayer/target/chronoplayer-1.0.jar ${INSTALL_DIR}/lib/
cd ${HLOG_DIR}
echo "Compilation Complete"
