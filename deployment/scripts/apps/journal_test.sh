#!/bin/bash
SCRIPT_DIR=`pwd`
exec_dir=/home/hdevarajan/projects/chronolog/build
cd $SCRIPT_DIR

./journal_s.sh
./journal_r.sh
mpirun -n 160 -f /home/hdevarajan/projects/chronolog/deployment/scripts/conf/server_lists/clients ${exec_dir}/chronolog_1_journal_comp_shared /home/hdevarajan/projects/chronolog/deployment/scripts/conf/config/chronolog.json 2048 64 0 >> 8_j.log

./journal_s.sh
./journal_r.sh
mpirun -n 160 -f /home/hdevarajan/projects/chronolog/deployment/scripts/conf/server_lists/clients ${exec_dir}/chronolog_1_journal_comp_shared /home/hdevarajan/projects/chronolog/deployment/scripts/conf/config/chronolog.json 128 1024 0 >> 8_j.log

./journal_s.sh
./journal_r.sh
mpirun -n 160 -f /home/hdevarajan/projects/chronolog/deployment/scripts/conf/server_lists/clients ${exec_dir}/chronolog_1_journal_comp_shared /home/hdevarajan/projects/chronolog/deployment/scripts/conf/config/chronolog.json 64 2048 0 >> 8_j.log

./journal_s.sh
./journal_r.sh
mpirun -n 160 -f /home/hdevarajan/projects/chronolog/deployment/scripts/conf/server_lists/clients ${exec_dir}/chronolog_1_journal_comp_shared /home/hdevarajan/projects/chronolog/deployment/scripts/conf/config/chronolog.json 32 4096 0 >> 8_j.log
