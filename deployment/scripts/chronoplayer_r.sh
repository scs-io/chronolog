#!/bin/bash
SCRIPT_DIR=`pwd`
echo "Starting Chronoplayer"
echo "flink run -v $INSTALL_DIR/lib/chronoplayer-1.0.jar ${SCRIPT_DIR}/conf/config/chronoplayer.json" > ${SCRIPT_DIR}/chronoplayer.log
JOB_MANAGERS=($(cat $INSTALL_DIR/conf/masters))
JOB_MANAGER=${JOB_MANAGERS[0]}
ssh $JOB_MANAGER /bin/bash << EOF
nohup flink run -v $INSTALL_DIR/lib/chronoplayer-1.0.jar ${SCRIPT_DIR}/conf/config/chronoplayer.json >> ${SCRIPT_DIR}/chronoplayer.log 2>&1 &
CHRONOPLAYER_PID=\$!
echo \$CHRONOPLAYER_PID
echo \$CHRONOPLAYER_PID > ${SCRIPT_DIR}/chronoplayer.lock
EOF
