#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/flink_procs)
ANODES=($(cat ${SCRIPT_DIR}/conf/server_lists/flink_procs))
JOB_MANAGER=${ANODES[0]}
FILE="${INSTALL_DIR}/conf/slaves"
> "$FILE"
echo "${JOB_MANAGER}" > ${INSTALL_DIR}/conf/master
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
if [ $node_name == $node_procs ]; then
	echo $node_name >> $FILE    
else
    for ((i=1; i<=${node_procs}; i++)) 
    do
    	echo $node_name >> $FILE
    done
fi
done
