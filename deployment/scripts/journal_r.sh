#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/journal)
DIR=$(cat ${SCRIPT_DIR}/conf/config/chronolog.json | python -c "import sys, json; print json.load(sys.stdin)['DJOURNAL_DIR']")
procs=0
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
if [ $node_name == $node_procs ]; then
    procs=$(($procs+1))
else
    procs=$((procs + node_procs))
fi
ssh $node_name /bin/bash << EOF
rm -rf ${DIR}
mkdir -p ${DIR}
EOF
done
echo "Starting Journal"
echo "mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/journal ${HLOG_PROJECT_DIR}/build/journal/journal_server ${SCRIPT_DIR}/conf/config/chronolog.json" > ${SCRIPT_DIR}/journal.log
nohup mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/journal ${HLOG_PROJECT_DIR}/build/journal/journal_server ${SCRIPT_DIR}/conf/config/chronolog.json >> ${SCRIPT_DIR}/journal.log 2>&1 &
JOURNAL_PID=$!
echo $JOURNAL_PID > ${SCRIPT_DIR}/journal.lock
