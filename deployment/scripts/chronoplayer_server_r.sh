#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/chronoplayer_server)
procs=0
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
if [ $node_name == $node_procs ]; then
    procs=$(($procs+1))
else
    procs=$((procs + node_procs))
fi
done
echo "Starting Chronoplayer server"
echo "mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/chronoplayer_server ${HLOG_PROJECT_DIR}/build/chronoplayer/chronoplayer_server ${SCRIPT_DIR}/conf/config/chronolog.json" > ${SCRIPT_DIR}/chronoplayer_server.log
nohup mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/chronoplayer_server ${HLOG_PROJECT_DIR}/build/chronoplayer/chronoplayer_server ${SCRIPT_DIR}/conf/config/chronolog.json >> ${SCRIPT_DIR}/chronoplayer_server.log 2>&1 &
PID=$!
echo $PID > ${SCRIPT_DIR}/chronoplayer_server.lock
