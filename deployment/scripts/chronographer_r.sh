#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/flink_procs)
procs=0
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
if [ $node_name == $node_procs ]; then
    procs=$(($procs+1))
else
    procs=$((procs + node_procs))
fi
done
procs=$((procs*8))
echo "Starting Chronographer"
echo "flink run -p $procs -v $INSTALL_DIR/lib/chronographer-1.0.jar ${SCRIPT_DIR}/conf/config/chronographer.json" > ${SCRIPT_DIR}/chronographer.log
JOB_MANAGERS=($(cat $INSTALL_DIR/conf/masters))
JOB_MANAGER=${JOB_MANAGERS[0]}
ssh $JOB_MANAGER /bin/bash << EOF
nohup flink run -p $procs -v $INSTALL_DIR/lib/chronographer-1.0.jar ${SCRIPT_DIR}/conf/config/chronographer.json >> ${SCRIPT_DIR}/chronographer.log 2>&1 &
CHRONOGRAPHER_PID=\$!
echo \$CHRONOGRAPHER_PID
echo \$CHRONOGRAPHER_PID > ${SCRIPT_DIR}/chronographer.lock
EOF
