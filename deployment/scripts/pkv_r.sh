#!/bin/bash
SCRIPT_DIR=`pwd`
NODES=$(cat ${SCRIPT_DIR}/conf/server_lists/pkv)
PKV_DIR=$(cat ${SCRIPT_DIR}/conf/config/chronolog.json | python -c "import sys, json; print json.load(sys.stdin)['PKV_SERVER_DIR']")
procs=0
for node in $NODES
do
node_name="$(cut -d':' -f1 <<<$node)"
node_procs="$(cut -d':' -f2 <<<$node)"
if [ $node_name == $node_procs ]; then
    procs=$(($procs+1))
else
    procs=$((procs + node_procs))
fi
ssh $node_name /bin/bash << EOF
rm -rf ${PKV_DIR}
mkdir -p ${PKV_DIR}
EOF
done
echo "Starting PKV Server"
echo "mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/pkv ${HLOG_PROJECT_DIR}/build/pkv/pkv_server ${SCRIPT_DIR}/conf/config/chronolog.json" > ${SCRIPT_DIR}/pkv.log
nohup mpirun -n $procs -f ${SCRIPT_DIR}/conf/server_lists/pkv ${HLOG_PROJECT_DIR}/build/pkv/pkv_server ${SCRIPT_DIR}/conf/config/chronolog.json >> ${SCRIPT_DIR}/pkv.log 2>&1 &
PID=$!
echo $PID > ${SCRIPT_DIR}/pkv.lock
