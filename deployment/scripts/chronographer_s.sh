#!/bin/bash
SCRIPT_DIR=`pwd`
JOB_MANAGERS=($(cat $INSTALL_DIR/conf/masters))
JOB_MANAGER=${JOB_MANAGERS[0]}
ssh $JOB_MANAGER /bin/bash << EOF
kill `cat ${SCRIPT_DIR}/chronographer.lock`
rm ${SCRIPT_DIR}/chronographer.lock
EOF
