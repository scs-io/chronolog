#!/bin/bash
rm -rf /mnt/common/hdevarajan/software/install/log/*
./generateFlinkSlave.sh
JOB_MANAGERS=($(cat $INSTALL_DIR/conf/masters))
JOB_MANAGER=${JOB_MANAGERS[0]}
ssh $JOB_MANAGER /bin/bash << EOF
${INSTALL_DIR}/bin/start-cluster.sh
EOF
