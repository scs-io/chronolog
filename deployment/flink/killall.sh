#!/bin/bash

CLIENTS=$(cat $HOME/chronolog/deployment/machines/clients)
JOB_MANAGER=$(cat $HOME/chronolog/deployment/machines/JobManager)
JOURNAL=$(cat $HOME/chronolog/deployment/machines/Journal)
PKV=$(cat $HOME/chronolog/deployment/machines/PkvFlink)

for i in "${CLIENTS[@]}"
do
	ssh -tt "$i" <<- EOF
    bash $HOME/chronolog/deployment/scripts/start/killclients.sh
    exit 0
EOF
done

for i in "${JOB_MANAGER[@]}"
do
	ssh -tt "$i" <<- EOF
    bash $HOME/chronolog/deployment/scripts/start/killflink.sh
    exit 0
EOF
done


for i in "${JOURNAL[@]}"
do
	ssh -tt "$i" <<- EOF
    bash $HOME/chronolog/deployment/scripts/start/killjournal.sh
    exit 0
EOF
done


for i in "${PKV[@]}"
do
	ssh -tt "$i" <<- EOF
    bash $HOME/chronolog/deployment/scripts/start/killpkv.sh
    exit 0
EOF
done
