#!/bin/bash
SCRIPT_DIR=`pwd`
FLINK_MAIN=$(cat ${SCRIPT_DIR}/machines/PfsFlink)
FLINK_SOURCES=$(cat ${SCRIPT_DIR}/machines/PkvFlink)

MAIN_PROCESS=16
SOURCES_PROCESS=8

FILE="${INSTALL_DIR}/conf/slaves"

> "$FILE"

for i in $FLINK_MAIN 
do
    for (( c=0; c<MAIN_PROCESS; c++ ))
    do
        echo "$i" >> "$FILE"
    done
done

for i in $FLINK_SOURCES
do
    for (( c=0; c<SOURCES_PROCESS; c++ ))
    do
        echo "$i" >> "$FILE"
    done
done
