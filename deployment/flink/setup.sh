#!/bin/bash

CLIENTS=$(cat "$HOME"/chronolog/deployment/machines/clients)
JOB_MANAGER=$(cat "$HOME"/chronolog/deployment/machines/JobManager)
JOURNAL=$(cat "$HOME"/chronolog/deployment/machines/Journal)
PKV=$(cat $HOME/chronolog/deployment/machines/PkvFlink)


bash $HOME/chronolog/deployment/scripts/start/clients.sh 10000 4096


for i in "${JOURNAL[@]}"
do
	ssh -tt "$i" <<- "EOF"
    bash $HOME/chronolog/deployment/scripts/start/journal.sh
    exit 0
EOF
done


for i in "${PKV[@]}"
do
	ssh -tt "$i" <<- "EOF"
    bash $HOME/chronolog/deployment/scripts/start/pkv.sh
    exit 0
EOF
done

for i in "${JOB_MANAGER[@]}"
do
	ssh -tt "$i" <<- "EOF"
    bash $HOME/chronolog/deployment/scripts/start/flink.sh
    exit 0
EOF
done



