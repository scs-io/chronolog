#!/bin/bash

NUM_THREADS=8

HOST=$(hostname)

FILE="$HOME/chronolog/deployment/pid/pkv_${HOST}.pid"

echo "" > "$FILE"

for (( c=0; c<=NUM_THREADS; c++ ))
do
  /"$HOME"/chronolog/cmake-build-debug/pkv_server &
  echo $! >> "$FILE"
done