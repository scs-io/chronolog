#!/bin/bash

NUM_THREADS=10

HOST=$(hostname)

FILE="$HOME/chronolog/deployment/pid/journal_${HOST}.pid"

echo "" > "$FILE"

for (( c=0; c<=NUM_THREADS; c++ ))
do
  /"$HOME"/chronolog/cmake-build-debug/djournal_server &
  echo $! >> "$FILE"
done