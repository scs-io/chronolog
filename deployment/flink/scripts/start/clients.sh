#!/bin/bash

HOST=$(hostname)
CLIENTS=$(cat "$HOME"/chronolog/deployment/machines/clients)

#FILE="$HOME/chronolog/deployment/pid/client_${HOST}.pid"

echo "" > "$FILE"

./mpiexec -n 28 $CLIENTS /"$HOME"/chronolog/cmake-build-debug/hlog_flink_test $1 $2 &

#echo $! >> "$FILE"