#!/bin/bash

HOST=$(hostname)

FILE="$HOME/chronolog/deployment/pid/journal_${HOST}.pid"
PIDS=$(cat $FILE)

for i in "${PIDS[@]}"
do
	kill -9 $i
done
