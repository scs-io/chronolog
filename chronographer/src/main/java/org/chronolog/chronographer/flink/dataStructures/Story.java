package org.chronolog.chronographer.flink.dataStructures;

import java.util.ArrayList;

import jchronolog.utils.Event;

public class Story{
    public int storyId;
    public ArrayList<Event> events;

    public Story(int storyId, ArrayList<Event>  events) {
        this.storyId = storyId;
        this.events = events;
    }

    public String getChronicleName(){
        return events.get(0).journal_name;
    }
}
