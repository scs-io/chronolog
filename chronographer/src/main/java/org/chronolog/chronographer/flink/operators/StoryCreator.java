package org.chronolog.chronographer.flink.operators;

import jchronolog.utils.Event;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.chronolog.chronographer.flink.dataStructures.Story;

import java.util.ArrayList;

public class StoryCreator extends KeyedProcessFunction<String, Event, Story> {
    private ValueState<ArrayList<Event>> listState = null;
    private ValueState<Integer> storedStoryId = null;
    private ValueState<Long> timerTime = null;

    private int max_size;
    private int max_timeout;

    public StoryCreator(int max_size, int max_timeout){
        this.max_size = max_size;
        this.max_timeout = max_timeout;
    }

    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<ArrayList<Event>> descriptor = new ValueStateDescriptor<>(
                "sorted-events",
                TypeInformation.of(new TypeHint<ArrayList<Event>>() {
                }));
        listState = getRuntimeContext().getState(descriptor);

        ValueStateDescriptor<Integer> descriptorInt = new ValueStateDescriptor<>(
                "story-id",
                TypeInformation.of(new TypeHint<Integer>() {
                }));
        storedStoryId = getRuntimeContext().getState(descriptorInt);

        ValueStateDescriptor<Long> descriptorLong = new ValueStateDescriptor<>(
                "Time_Timer",
                TypeInformation.of(new TypeHint<Long>() {
                }));
        timerTime = getRuntimeContext().getState(descriptorLong);
    }

    @Override
    public void processElement(Event event, Context ctx, Collector<Story> out) throws Exception {
        TimerService timerService = ctx.timerService();
        Long coalescedTime;
        ArrayList<Event> list = listState.value();
        if (list == null) {
            list = new ArrayList<>();
            coalescedTime = System.currentTimeMillis() + max_timeout;
            timerTime.update(coalescedTime);
            ctx.timerService().registerProcessingTimeTimer(coalescedTime);
        }

        list.add(event);
        listState.update(list);

        if(list.size() >= max_size){
            coalescedTime = timerTime.value();
            ctx.timerService().deleteProcessingTimeTimer(coalescedTime);

            Integer storyId = storedStoryId.value();
            if(storyId == null){
                storyId = -1;
            }
            storyId++;
            storedStoryId.update(storyId);
            listState.update(null);
            out.collect(new Story(storyId, list));
        }
    }

    //TODO: Search a little bit more on how this works. Like what happens if it triggers while on line 71 or something
    //TODO: Also, I think im disabling it correctly but im not 100% sure.

    @Override
    public void onTimer(long timestamp, OnTimerContext context, Collector<Story> out) throws Exception {
        ArrayList<Event> list = listState.value();

        Integer storyId = storedStoryId.value();

        if(list.size() > 0) {
            if(storyId == null){
                storyId = -1;
            }
            storyId++;
            storedStoryId.update(storyId);
            listState.update(null);
            out.collect(new Story(storyId, list));
        }
    }
}
