package org.chronolog.chronographer.flink;

import jchronolog.utils.Event;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.chronolog.chronographer.flink.dataStructures.Story;
import org.chronolog.chronographer.flink.operators.JournalSource;
import org.chronolog.chronographer.flink.operators.PFSSink;
import org.chronolog.chronographer.flink.operators.StoryCreator;
import org.chronolog.chronographer.flink.operators.StorySorter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class Chronographer {

	public static void main(String[] args) throws Exception {
		/**
		 * TODO: take it as input args
		 */

		JSONParser parser = new JSONParser();

		FileReader fileReader;
		if(args.length == 0){
			fileReader = new FileReader("config.json");
		}
		else {
			fileReader = new FileReader(args[0]);
		}
		JSONObject json = (JSONObject) parser.parse(fileReader);

		String HDDPath = (String) json.get("file_path");
		int max_nvme_servers = ((Long)json.get("max_servers")).intValue();
		int max_size_story = ((Long)json.get("max_size_story")).intValue();
		int max_timeout_story = ((Long)json.get("max_timeout_story")).intValue();
		int num_threads_sort = ((Long)json.get("num_threads_sort")).intValue();
		int timeout_pull = ((Long)json.get("timeout_pull")).intValue();
		String conf_path = (String) json.get("conf_path");

		System.out.println("");
		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStream<Event> storyEntriesSources=env.addSource(new JournalSource(0, conf_path));
		for(int i=1;i<max_nvme_servers;++i){
			storyEntriesSources = storyEntriesSources.union(env.addSource(new JournalSource(i, conf_path)));
		}
		KeyedStream<Event, String> storyEntries = storyEntriesSources.keyBy(event -> event.journal_name);

        KeyedStream<Story, Integer> stories = storyEntries
				.process(new StoryCreator(max_size_story, max_timeout_story))
                .keyBy(story -> story.storyId);

		KeyedStream<Story, Integer> sortedStories = stories
				.flatMap(new StorySorter(num_threads_sort))
				.keyBy(story -> story.storyId);

		sortedStories.addSink(new PFSSink(HDDPath, conf_path));

		env.execute("Chronographer");
	}
}