package org.chronolog.chronographer.flink.operators;

import jchronolog.chronolog.Chronolog;
import jchronolog.pkv.PKV;
import jchronolog.pwriter.PWriter;
import jchronolog.utils.Event;
import jchronolog.utils.Location;
import jchronolog.utils.Pair;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.chronolog.chronographer.flink.dataStructures.Story;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class PFSSink extends RichSinkFunction<Story> {

    private static Semaphore pfsSync = new Semaphore(1);
    private PKV pkvClient;
    private Chronolog hlog_client;
    private String path;
    private String conf_path;

    private ValueState<ByteArrayOutputStream> bufferState;
    private ValueState<Integer> fileOffsetState;
    private ValueState<Integer> bufferOffsetState;

    public PFSSink(String path, String conf_path) {

        this.path = path;
        this.conf_path = conf_path;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void open(Configuration parameters) throws InterruptedException {
        pfsSync.acquire();
        ValueStateDescriptor<ByteArrayOutputStream> bufferDescriptor = new ValueStateDescriptor<>(
                "bufferState",
                TypeInformation.of(new TypeHint<ByteArrayOutputStream>() {
                }), new ByteArrayOutputStream());
        bufferState = getRuntimeContext().getState(bufferDescriptor);

        ValueStateDescriptor<Integer> fileOffsetDescriptor = new ValueStateDescriptor<>(
                "fileOffsetState",
                TypeInformation.of(new TypeHint<Integer>() {
                }), 0);
        fileOffsetState = getRuntimeContext().getState(fileOffsetDescriptor);

        ValueStateDescriptor<Integer> bufferOffsetDescriptor = new ValueStateDescriptor<>(
                "fileOffsetState",
                TypeInformation.of(new TypeHint<Integer>() {
                }), 0);
        bufferOffsetState = getRuntimeContext().getState(bufferOffsetDescriptor);


        /**
         * instantiate client
         */
        pkvClient=new PKV();
        hlog_client=new Chronolog();
        /**
         * MPI_init
         */
        pkvClient.pkvInit(conf_path);;
        /**
         * instantiate client singleton
         */
        pkvClient.pkvInitClient("SSD_KV");
        hlog_client.InitMetadata();

        pfsSync.release();
    }

    @Override
    public void close() throws InterruptedException {
        hlog_client.Exit();
    }

    @Override
    public void invoke(Story value,  SinkFunction.Context context) throws IOException, InterruptedException {
        ByteArrayOutputStream buffer = bufferState.value();

        final int fileOffset = fileOffsetState.value();
        int bufferOffset = bufferOffsetState.value();
        String fileName=path + "/" + value.getChronicleName() + "_" + value.storyId + ".events";
        int[] statusArray =new int[value.events.size()];
        int i=0;
        for (Event event : value.events) {
            Pair<Boolean,String> data_pair=pkvClient.pkvGet(event);
            if(data_pair.getFirst()){
                byte[] eventByte = data_pair.getSecond().getBytes();
                int sizeEvent = eventByte.length;
                buffer.write(eventByte, 0, sizeEvent);
                Location location=new Location();
                location.path=fileName;
                location.offset=fileOffset + bufferOffset;
                location.event_size=sizeEvent;

                statusArray[i] = hlog_client.MetadataPut(event,location);
                if(statusArray[i]==0) bufferOffset += sizeEvent;
                i++;
            }
        }
        Location final_location=new Location();
        final_location.path=fileName;
        final_location.offset=fileOffset;
        final_location.event_size=bufferOffset;
        PWriter pWriter=new PWriter();
        pWriter.Append(final_location,buffer.toString());
        fileOffsetState.update(fileOffset + bufferOffset);
        buffer.reset();
        bufferOffsetState.update(0);
        bufferState.update(buffer);
        for(i=0;i<value.events.size();++i){
            if(statusArray[i]==0) pkvClient.pkvDelete(value.events.get(i));
            long end=System.currentTimeMillis();
            System.out.println("Start,"+value.events.get(i).initial_timestamp+",End,"+end+",Duration,"+(end - value.events.get(i).initial_timestamp));

        }
    }
}
