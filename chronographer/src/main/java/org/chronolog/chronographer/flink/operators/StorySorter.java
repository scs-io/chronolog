package org.chronolog.chronographer.flink.operators;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.chronolog.chronographer.flink.dataStructures.Story;

import java.util.Comparator;

public class StorySorter implements FlatMapFunction<Story, Story> {

    private int num_threads;

    public StorySorter(int num_threads){
        this.num_threads = num_threads;
    }

    @Override
    public void flatMap(Story value, Collector<Story> out) {
        ParallelMergeSorter.sort(value.events, Comparator.naturalOrder(), num_threads);
        out.collect(value);
    }
}

