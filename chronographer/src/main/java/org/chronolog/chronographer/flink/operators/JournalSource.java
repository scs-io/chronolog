package org.chronolog.chronographer.flink.operators;

import jchronolog.journal.Journal;
import jchronolog.pkv.PKV;
import jchronolog.utils.Event;
import jchronolog.utils.Pair;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class JournalSource extends RichSourceFunction<Event> {

    private Journal journalClient;

    private PKV pkvClient;
    private int count;
    private int server_index;
    private String conf_path;
    private static Semaphore journal = new Semaphore(1);

    public JournalSource(int server_index, String conf_path) throws InterruptedException{
        this.server_index=server_index;
        this.conf_path=conf_path;
    }

    public void open(Configuration parameters) throws InterruptedException {

        /**
         * instantiate client
         */
        journalClient=new Journal();
        pkvClient=new PKV();
        /**
         * MPI_init
         */
        journal.acquire();
        journalClient.djInit(conf_path);
        /**
         * instantiate client singleton
         */
        pkvClient.pkvInitClient("SSD_KV");
        journalClient.djInitClient();
        journal.release();
    }

    @Override
    public void run(SourceContext<Event> sourceContext) throws InterruptedException{
        Event msg=new Event();
        msg.event_id=0;
        int count=0;
        while(true) {
            try {
                Thread.sleep(100);
                ArrayList<Pair<Event, String>> oldest_datas = journalClient.djRetrieveOldestEntry(server_index);
                if(oldest_datas.size() == 0) continue;
                System.out.println("Got chronicles: " + oldest_datas.size());

                for(Pair<Event, String> eventPair :oldest_datas){
                    eventPair.getFirst().initial_timestamp = System.currentTimeMillis();
                    boolean status = pkvClient.pkvSet(eventPair.getFirst(),eventPair.getSecond());
                    journalClient.djOpen(eventPair.getFirst().journal_name);
                    if(status) journalClient.djDeleteEntry(eventPair.getFirst());
                    else System.out.println("Cant Delete key : "+eventPair.getFirst().event_id);
                    journalClient.djClose(eventPair.getFirst().journal_name);
                    count++;
                    if (eventPair.getSecond().equals("END")) {
                        break;
                    } else {
                        sourceContext.collect(eventPair.getFirst());
                    }
                }
            }
            catch(Exception e){
                count++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                if(count > 5){
                    System.out.println("closing");
                    break;
                }
            }
        }
    }

    @Override
    public void cancel() {
        try{
            journalClient.djExit();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }
}