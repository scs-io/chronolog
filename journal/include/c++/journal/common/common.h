//
// Created by hariharan on 11/13/19.
//

#ifndef CHRONOLOG_JOURNAL_COMMON_H
#define CHRONOLOG_JOURNAL_COMMON_H


#include <chronolog/common/data_structure.h>

namespace std {
    template<>
    struct hash<Event> {
        int operator()(const Event &k) const {
            size_t hash_val = hash<CharStruct>()(k.journal_name_);
            hash_val ^= hash<uint32_t>()(k.event_id);
            return hash_val;
        }
    };
}
#endif //CHRONOLOG_JOURNAL_COMMON_H
