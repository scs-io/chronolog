//
// Created by hariharan on 8/14/19.
//

#ifndef DJOURNAL_SERVER_H
#define DJOURNAL_SERVER_H

#include "chronolog/common/data_structure.h"
#include <c++/journal/common/common.h>
#include <unordered_map>
#include <basket/common/data_structures.h>
#include <basket/unordered_map/unordered_map.h>
#include <basket/set/set.h>
#include <rpc/client.h>
#include <boost/thread/mutex.hpp>

namespace bip=boost::interprocess;

namespace journal {
    class Server {
        std::unordered_map<CharStruct,std::shared_ptr<basket::unordered_map<Event,bip::string>>> data_map;
        std::unordered_map<CharStruct,std::shared_ptr<basket::set<Event>>> meta_map;
        basket::set<Event> backlog;
        basket::unordered_map<CharStruct,std::vector<uint32_t>> tail_map;
        std::promise<void> exit_server_signal;
        std::thread worker;
        int num_servers;
        std::shared_ptr<RPC> client_rpc,flink_rpc, internal_rpc;
        boost::mutex io_mutex;
        void RunInternal(std::future<void> futureObj);
        void CreateSpace(really_long required_space);
        really_long GetSize();
    public:
        Server();
        int IntializeJournal(CharStruct &journal_name);
        int DestroyJournal(CharStruct &journal_name);
        bool CheckJournal(CharStruct &journal_name);
        int BindPutCallback(CharStruct &journal_name);
        bool PutDataCallback(Event &event,std::string &data);
        std::vector<std::pair<Event,std::string>> RetrieveOldestEntryLocal();
        std::vector<std::pair<Event,std::string>> GetRange(Event& start_key,Event& end_key);
        std::vector<std::pair<Event,std::string>> GetRangeLocal(Event& start_key,Event& end_key);
        int LocalUpdateTail(CharStruct &journal_name, uint32_t &event_id);
        int UpdateTail(CharStruct &journal_name, uint32_t &event_id);
        void Run(){
            std::future<void> futureObj = exit_server_signal.get_future();
            worker=std::thread (&Server::RunInternal, this, std::move(futureObj));
        }
        void Stop(){
            exit_server_signal.set_value();
            worker.join();
            if(BASKET_CONF->MPI_RANK == 0) printf("Stopped the Journal server\n");
        }
    };
}


#endif //HLOG_SERVER_H
