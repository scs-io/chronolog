//
// Created by hariharan on 8/14/19.
//

#ifndef DJOURNAL_CLIENT_H
#define DJOURNAL_CLIENT_H

/**
 * Internal header
 */
#include "chronolog/common/data_structure.h"
#include <c++/journal/common/common.h>
/**
 * Standard headers
 */
#include <string>
/**
 * External library headers
 */
#include <basket/common/data_structures.h>
#include <basket/unordered_map/unordered_map.h>
#include <basket/set/set.h>
#include <boost/thread/mutex.hpp>

#include <rpc/client.h>
namespace bip=boost::interprocess;

namespace journal{
    class Client {
    private:
        std::unordered_map<CharStruct,std::shared_ptr<basket::unordered_map<Event,bip::string>>> data_map;
        std::unordered_map<CharStruct,std::shared_ptr<basket::set<Event>>> meta_map;
        basket::unordered_map<CharStruct,std::vector<uint32_t>> tail_map;
        basket::set<Event> backlog;
        std::shared_ptr<RPC> rpc;
        int num_servers;
        boost::mutex mtx;
    public:

        Client();
        int Create(CharStruct &journal_name);
        int Destroy(CharStruct &journal_name);
        int Open(CharStruct &journal_name);
        int Close(CharStruct &journal_name);
        int Append(Event &event,std::string &data);
        int Tail(Event &event,std::string &data);
        int Retrieve(Event &event,std::string &data);
        int RetrieveRange(CharStruct &journal_name, uint32_t & start_event_id, uint32_t & end_event_id,std::vector<std::pair<Event, std::string>> &events);
        int RetrieveOldestEntry(CharStruct &journal_name, Event& event, uint16_t server_index);
        int BacklogEntry(Event& event);
        int DeleteEntry(Event& event);
        int CheckJournal(CharStruct &journal_name);
        std::vector<std::pair<Event,std::string>> RetrieveOldestEntry(uint16_t server_index);
    };
}



#endif //DJOURNAL_CLIENT_H
