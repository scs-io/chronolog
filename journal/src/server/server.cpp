//
// Created by hariharan on 8/14/19.
//

#include <c++/journal/server/server.h>
#include <chronolog/common/configuration_manager.h>

journal::Server::Server(): data_map(),
                            meta_map(),
                            tail_map("TAIL_MAP",BASKET_CONF->RPC_PORT),
                            backlog("BACKLOG_SET",CHRONOLOG_CONF->DJOURNAL_SERVER_OUT_PORT),
                            exit_server_signal(),
                            worker(),
                            num_servers(BASKET_CONF->NUM_SERVERS) {
    client_rpc = basket::Singleton<RPCFactory>::GetInstance()->GetRPC(CHRONOLOG_CONF->DJOURNAL_SERVER_PORT);
    flink_rpc = basket::Singleton<RPCFactory>::GetInstance()->GetRPC(CHRONOLOG_CONF->DJOURNAL_SERVER_OUT_PORT);
    internal_rpc = basket::Singleton<RPCFactory>::GetInstance()->GetRPC(CHRONOLOG_CONF->DJOURNAL_SERVER_INT_PORT);

    std::function<int(CharStruct&)> functionIntializeJournal(std::bind(&journal::Server::IntializeJournal,this,std::placeholders::_1));
    std::function<int(CharStruct&)> functionDestroyJournal(std::bind(&journal::Server::DestroyJournal,this,std::placeholders::_1));
    std::function<bool(CharStruct&)> functionCheckJournal(std::bind(&journal::Server::CheckJournal,this,std::placeholders::_1));
    std::function<std::vector<std::pair<Event,std::string>>(Event&,Event&)> functionGetRangeLocal(std::bind(&Server::GetRangeLocal,this, std::placeholders::_1,
            std::placeholders::_2));
    std::function<std::vector<std::pair<Event,std::string>>(Event&,Event&)> functionGetRange(std::bind(&Server::GetRange,this,std::placeholders::_1,std::placeholders::_2));
    std::function<std::vector<std::pair<Event,std::string>>(void)> functionRetrieveOldestEntryLocal(std::bind(&journal::Server::RetrieveOldestEntryLocal,this));
    std::function<int(CharStruct &, uint32_t &)> functionLocalUpdateTail(std::bind(&Server::UpdateTail,this, std::placeholders::_1,
                                                                                   std::placeholders::_2));
    client_rpc->bind("CheckJournal", functionCheckJournal);
    client_rpc->bind("IntializeJournal", functionIntializeJournal);
    client_rpc->bind("DestroyJournal", functionDestroyJournal);
    client_rpc->bind("GetRangeLocal", functionGetRangeLocal);
    client_rpc->bind("GetRange", functionGetRange);
    client_rpc->bind("LocalUpdateTail", functionLocalUpdateTail);
    client_rpc ->bind("RetrieveOldestEntryLocal",functionRetrieveOldestEntryLocal);

    flink_rpc->bind("CheckJournal", functionCheckJournal);
    flink_rpc->bind("IntializeJournal", functionIntializeJournal);
    flink_rpc->bind("DestroyJournal", functionDestroyJournal);
    flink_rpc->bind("GetRangeLocal", functionGetRangeLocal);
    flink_rpc->bind("GetRange", functionGetRange);
    flink_rpc->bind("LocalUpdateTail", functionLocalUpdateTail);
    flink_rpc ->bind("RetrieveOldestEntryLocal",functionRetrieveOldestEntryLocal);

    internal_rpc->bind("CheckJournal", functionCheckJournal);
    internal_rpc->bind("IntializeJournal", functionIntializeJournal);
    internal_rpc->bind("DestroyJournal", functionDestroyJournal);
    internal_rpc->bind("GetRangeLocal", functionGetRangeLocal);
    internal_rpc->bind("GetRange", functionGetRange);
    internal_rpc->bind("LocalUpdateTail", functionLocalUpdateTail);
    internal_rpc->bind("RetrieveOldestEntryLocal",functionRetrieveOldestEntryLocal);
}

void journal::Server::RunInternal(std::future<void> futureObj) {
    bool count=true;
    while(futureObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
        usleep(10000);
        if(count){
            if(BASKET_CONF->MPI_RANK == 0) printf("Started the Journal server\n");
            count=false;
        }
    }
}

void journal::Server::CreateSpace(really_long required_space) {
    const really_long NVME_CAP = CHRONOLOG_CONF->NVME_CAPACITY;
    really_long current_size = GetSize();
    if(NVME_CAP - current_size > required_space)
        return;
    val:
    while(backlog.data()->empty()) usleep(10);
    auto iter = backlog.data()->begin();
    while (iter != backlog.data()->end() && required_space > 0) {
        io_mutex.lock();
        auto data_iter = data_map.find(iter->journal_name_);
        if (data_iter != data_map.end()) {
            really_long reduced_size = data_iter->second->size_occupied;
            auto val = data_iter->second->Erase(*iter);
            reduced_size -= data_iter->second->size_occupied;
            if(required_space < reduced_size) required_space = 0;
            else required_space -= reduced_size;
        }
        auto meta_iter = meta_map.find(iter->journal_name_);
        if (meta_iter != meta_map.end()) meta_iter->second->Erase(*iter);
        backlog.LocalErase(*iter);
        io_mutex.unlock();
    }
    if(required_space > 0) goto val;
}

really_long journal::Server::GetSize() {
    really_long size=0;
    io_mutex.lock();
    for(auto data_iter = data_map.begin();data_iter!=data_map.end();++data_iter){
        size+=data_iter->second->size_occupied;
    }
    io_mutex.unlock();
    return size;
}

int journal::Server::IntializeJournal(CharStruct &journal_name) {
    if(CheckJournal(journal_name))
        return -1;
    auto map = make_shared<basket::unordered_map<Event,bip::string>>(journal_name+"_DATA");
    auto set = make_shared<basket::set<Event>>(journal_name+"_META");
    io_mutex.lock();
    data_map.insert_or_assign(journal_name,map);
    meta_map.insert_or_assign(journal_name,set);
    io_mutex.unlock();
    BindPutCallback(journal_name);
    return 0;
}

int journal::Server::DestroyJournal(CharStruct &journal_name) {
    io_mutex.lock();
    auto data_iter = data_map.find(journal_name);
    if(data_iter!=data_map.end()) data_map.erase(journal_name);
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter!=meta_map.end()) meta_map.erase(journal_name);
    io_mutex.unlock();
    return 0;
}

bool journal::Server::CheckJournal(CharStruct &journal_name) {
    io_mutex.lock();
    auto data_iter = data_map.find(journal_name);
    if(data_iter==data_map.end()){
        io_mutex.unlock();
        return false;
    }
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter==meta_map.end()){
        io_mutex.unlock();
        return false;
    }
    io_mutex.unlock();
    return true;
}

bool journal::Server::PutDataCallback(Event &event,std::string &data) {

    CreateSpace(CalculateSize<Event>().GetSize(event) + CalculateSize<std::string>().GetSize(data));
    io_mutex.lock();
    auto data_iter = data_map.find(event.journal_name_);
    bip::string b_data(data.c_str());
    if(data_iter!=data_map.end())
        data_iter->second->Put(event,b_data);
    auto meta_iter = meta_map.find(event.journal_name_);
    if(meta_iter!=meta_map.end())
        meta_iter->second->Put(event);
    io_mutex.unlock();
    UpdateTail(event.journal_name_, event.event_id);
    return true;
}

int journal::Server::BindPutCallback(CharStruct &journal_name) {
    std::function<bool(Event&,std::string&)> functionPutDataCallback(std::bind(&Server::PutDataCallback,this,std::placeholders::_1,std::placeholders::_2));
    client_rpc->bind(journal_name + "_PutDataCallback", functionPutDataCallback);
    return 0;
}

std::vector<std::pair<Event, std::string>> journal::Server::GetRange(Event &start_key, Event &end_key) {
    auto final_values=std::vector<std::pair<Event,std::string>>();
    auto current_server = GetRangeLocal(start_key,end_key);
    final_values.insert(final_values.end(), current_server.begin(), current_server.end());
    for (int i = 0; i < BASKET_CONF->COMM_SIZE; ++i) {
        if (i != BASKET_CONF->MPI_RANK) {
            typedef std::vector<std::pair<Event,std::string>> ret_type;
            auto server = internal_rpc->call<RPCLIB_MSGPACK::object_handle>(i, "GetRangeLocal", start_key, end_key).as<std::vector<std::pair<Event, std::string>>>();
            final_values.insert(final_values.end(), server.begin(), server.end());
        }
    }
    return final_values;
}

std::vector<std::pair<Event, std::string>> journal::Server::GetRangeLocal(Event &start_key, Event &end_key) {
    auto final_values=std::vector<std::pair<Event, std::string>>();
    io_mutex.lock();
    auto meta_iter = meta_map.find(start_key.journal_name_);
    auto data_iter = data_map.find(start_key.journal_name_);
    if(meta_iter!=meta_map.end() && data_iter!=data_map.end()){
        auto matched_keys = meta_iter->second->ContainsInServer(start_key,end_key);
        for(auto matched_key:matched_keys){
            final_values.insert(final_values.end(), std::pair<Event, std::string>(matched_key,data_iter->second->Get(matched_key).second));
        }
    }
    io_mutex.unlock();
    return final_values;
}

std::vector<std::pair<Event,std::string>> journal::Server::RetrieveOldestEntryLocal() {
    auto return_events=std::vector<std::pair<Event,std::string>>();
    io_mutex.lock();
    for(auto meta_iter = meta_map.begin();meta_iter!=meta_map.end();++meta_iter){
        auto data_iter = data_map.find(meta_iter->first);
        if(data_iter == data_map.end()) continue;
        auto size = meta_iter->second->LocalSize();
        if(size == 0) continue;
        uint32_t amount =  size;
        auto seek_iter = meta_iter->second->data()->begin();
        while(seek_iter != meta_iter->second->data()->end() && amount > 0){
            if(backlog.data()->find(*seek_iter)==backlog.data()->end()){
                auto data_return = data_iter->second->Get(*seek_iter);
                if(!data_return.first) continue;
                return_events.push_back(std::pair<Event,std::string>(*seek_iter,data_return.second));
                amount--;
            }
            seek_iter++;
        }
    }
    io_mutex.unlock();
    return return_events;
}

int journal::Server::LocalUpdateTail(CharStruct &journal_name, uint32_t &event_id) {
    io_mutex.lock();
    auto map = tail_map.data();
    auto iterator = map->find(journal_name);
    if (iterator != map->end()) {
        iterator->second[BASKET_CONF->MY_SERVER]=event_id;
    }
    io_mutex.unlock();
    return 0;
}

int journal::Server::UpdateTail(CharStruct &journal_name,uint32_t &event_id) {
    size_t server_index = std::hash<CharStruct>()(journal_name) % num_servers;
    if(server_index == BASKET_CONF->MY_SERVER){
        return LocalUpdateTail(journal_name,event_id);
    }else{
        return internal_rpc->call<RPCLIB_MSGPACK::object_handle>(server_index, "LocalUpdateTail", journal_name, event_id).as<int>();
    }
}






