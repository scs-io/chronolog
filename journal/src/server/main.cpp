//
// Created by hariharan on 8/15/19.
//

#include <mpi.h>
#include <basket/common/macros.h>
#include <c++/journal/server/server.h>
#include <chronolog/common/configuration_manager.h>
#include <chronolog/common/daemonize.h>

void finalize(){
    MPI_Barrier(MPI_COMM_WORLD);
    auto server = basket::Singleton<journal::Server>::GetInstance();
    server->Stop();
    MPI_Finalize();
}

int main(int argc, char* argv[]){
    MPI_Init_thread(&argc,&argv,true,NULL);
    MPI_Barrier(MPI_COMM_WORLD);
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    BASKET_CONF->BACKED_FILE_DIR=CHRONOLOG_CONF->DJOURNAL_DIR;
    CHRONOLOG_CONF->ConfigureJournalServer();
    auto server = basket::Singleton<journal::Server>::GetInstance();
    server->Run();
    strcpy(main_log_file,"journal.log");
    catch_all_signals();
    while(true) sleep(1);
}
