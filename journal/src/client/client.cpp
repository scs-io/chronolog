//
// Created by hariharan on 8/14/19.
//

#include <c++/journal/client/client.h>
#include <chronolog/common/configuration_manager.h>

journal::Client::Client(): data_map(),
                           meta_map(),
                           tail_map("TAIL_MAP",BASKET_CONF->RPC_PORT),
                           backlog("BACKLOG_SET"),
                           num_servers(BASKET_CONF->NUM_SERVERS) {
    mtx.lock();
    rpc=basket::Singleton<RPCFactory>::GetInstance()->GetRPC(BASKET_CONF->RPC_PORT);
    mtx.unlock();
}

int journal::Client::Create(CharStruct &journal_name) {
    if(CheckJournal(journal_name) == 0)  return -1;
    auto server_index=rand()%num_servers;
    auto check = rpc->call<RPCLIB_MSGPACK::object_handle>(server_index, "CheckJournal", journal_name).as<bool>();
    if(check) return -1;
    auto servers_tails=std::vector<uint32_t>();
    for(int i=0;i<num_servers;++i){
        auto status = rpc->call<RPCLIB_MSGPACK::object_handle>(i, "IntializeJournal", journal_name).as<int>();
        if(status<0) return status;
        servers_tails.push_back(0);
    }
    mtx.lock();
    tail_map.Put(journal_name,servers_tails);
    /**
     * initialize client maps
     */
    CHRONOLOG_CONF->ConfigureJournalClient();
    auto map = make_shared<basket::unordered_map<Event,bip::string>>(journal_name+"_DATA");
    auto set = make_shared<basket::set<Event>>(journal_name+"_META");
    data_map.insert_or_assign(journal_name,map);
    meta_map.insert_or_assign(journal_name,set);
    mtx.unlock();
    return 0;
}

int journal::Client::Open(CharStruct &journal_name) {
    if(CheckJournal(journal_name) == 0)
        return -1;
    auto server_index=rand()%num_servers;
    auto check = rpc->call<RPCLIB_MSGPACK::object_handle>(server_index, "CheckJournal", journal_name).as<bool>();
    if(!check) return -1;
    /**
    * initialize client maps
    */
    mtx.lock();
    CHRONOLOG_CONF->ConfigureJournalClient();
    auto map = make_shared<basket::unordered_map<Event,bip::string>>(journal_name+"_DATA");
    auto set = make_shared<basket::set<Event>>(journal_name+"_META");
    data_map.insert_or_assign(journal_name,map);
    meta_map.insert_or_assign(journal_name,set);
    mtx.unlock();
    return 0;
}

int journal::Client::Close(CharStruct &journal_name) {
    if(CheckJournal(journal_name) == -1)
        return -1;
    mtx.lock();
    auto data_iter = data_map.find(journal_name);
    if(data_iter!=data_map.end()) data_map.erase(journal_name);
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter!=meta_map.end()) meta_map.erase(journal_name);
    mtx.unlock();
    return 0;
}

int journal::Client::Destroy(CharStruct &journal_name) {
    if(CheckJournal(journal_name) == -1)
        return -1;
    for(int i=0;i<num_servers;++i){
        auto status = rpc->call<RPCLIB_MSGPACK::object_handle>(i, "DestroyJournal", journal_name).as<int>();
        if(status<0) return status;
    }
    mtx.lock();
    auto data_iter = data_map.find(journal_name);
    if(data_iter!=data_map.end()) data_map.erase(journal_name);
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter!=meta_map.end()) meta_map.erase(journal_name);
    mtx.unlock();
    return 0;
}

int journal::Client::Append(Event &event, std::string &data) {
    uint16_t key_int = (uint16_t)std::hash<Event>()(event)% num_servers;
    auto status = rpc->call<RPCLIB_MSGPACK::object_handle>(key_int, event.journal_name_ + "_PutDataCallback", event, data).as<bool>();
    if(!status) return -1;
    else return 0;
}

int journal::Client::Tail(Event &event, std::string &data) {
    if(CheckJournal(event.journal_name_) == -1)
        return -1;
    uint32_t max=0;
    mtx.lock();
    auto server_tails = tail_map.Get(event.journal_name_);
    if(!server_tails.first){
        mtx.unlock();
        return -2;
    }
    for(auto server_tail : server_tails.second){
        if(max < server_tail) max = server_tail;
    }
    if(max==0){
        mtx.unlock();
        return -3;
    }
    event.event_id=max;
    auto data_iter = data_map.find(event.journal_name_);
    if(data_iter!=data_map.end()){
        auto iter= data_iter->second->Get(event);
        if(iter.first) data=iter.second;
    }else {
        mtx.unlock();
        return -4;
    }
    mtx.unlock();
    return 0;
}

int journal::Client::Retrieve(Event &event, std::string &data) {
    mtx.lock();
    auto data_iter = data_map.find(event.journal_name_);
    if(data_iter!=data_map.end()){
        auto iter= data_iter->second->Get(event);
        if(iter.first) data=iter.second;
        else{
            mtx.unlock();
            return -1;
        }
    }else {
        mtx.unlock();
        return -1;
    }
    mtx.unlock();
    return 0;
}

int journal::Client::RetrieveRange(CharStruct &journal_name, uint32_t &start_event_id, uint32_t &end_event_id, std::vector<std::pair<Event, std::string>> &events) {
    auto server_index=rand()%num_servers;
    Event e_start,e_end;
    e_start.journal_name_=journal_name;
    e_start.event_id=start_event_id;
    e_end.journal_name_=journal_name;
    e_end.event_id=end_event_id;
    events=rpc->call<RPCLIB_MSGPACK::object_handle>(server_index, "GetRange", e_start, e_end).as<std::vector<std::pair<Event, std::string>>>();
    sort(events.begin(), events.end(),
         [](const std::pair<Event, std::string> & a, const std::pair<Event, std::string> & b) -> bool{
             return a.first < b.first;
         });
    return 0;
}

int journal::Client::RetrieveOldestEntry(CharStruct &journal_name, Event &event, uint16_t server_index) {
    mtx.lock();
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter!=meta_map.end()){
        auto iter = meta_iter->second->SeekFirst(server_index);
        if(iter.first) event=iter.second;
        else {
            mtx.unlock();
            return -1;
        }
    }else {
        mtx.unlock();
        return -1;
    }
    mtx.unlock();
    return 0;
}

int journal::Client::DeleteEntry(Event &event) {
    mtx.lock();
    auto status = backlog.Put(event);
    mtx.unlock();
    return status?0:-1;
}
int journal::Client::BacklogEntry(Event &event) {
    mtx.lock();
    int status= backlog.Put(event)?0:-1;
    mtx.unlock();
    return status;
}



int journal::Client::CheckJournal(CharStruct &journal_name) {
    bool check=true;
    mtx.lock();
    auto data_iter = data_map.find(journal_name);
    if(data_iter==data_map.end()) check= false;
    auto meta_iter = meta_map.find(journal_name);
    if(meta_iter==meta_map.end()) check= false;
    mtx.unlock();
    if(!check) return -1;
    return 0;
}

std::vector<std::pair<Event,std::string>> journal::Client::RetrieveOldestEntry(uint16_t server_index) {
    return rpc->callWithTimeout<RPCLIB_MSGPACK::object_handle>(server_index,2000, "RetrieveOldestEntryLocal").as<std::vector<std::pair<Event,std::string>>>();
}

