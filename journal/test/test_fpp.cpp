//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <chronolog/common/configuration_manager.h>
#include "util.h"


int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    int  num_events=1000;
    size_t size_events=4096;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 0);
    if(argc > 3) size_events=std::strtol(argv[3], nullptr, 0);
    CHRONOLOG_CONF->ConfigureJournalClient();
    auto client = basket::Singleton<journal::Client>::GetInstance();
    char journal_name[20];
    snprintf(journal_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(journal_name);
    int status = client->Create(j_name);
    assert(status == 0);
    MPI_Barrier(MPI_COMM_WORLD);

    Event* events= static_cast<Event *>(malloc(sizeof(Event) * num_events));
    for(int i=0;i<num_events;++i){
        events[i]=Event();
        events[i].journal_name_= CharStruct(journal_name);
        events[i].event_id = GetTimeStamp();
        usleep(BASKET_CONF->MPI_RANK);
    }
    std::string data= random_string(size_events);
    for(int i=0;i<num_events;++i){
        status = client->Append(events[i], data);
        assert(status == 0);
	printf("\rPut Done %d",i);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK == 0) printf("\nAppend Done\n");
    for(int i=0;i<num_events;++i){
        std::string return_data;
        status = client->Retrieve(events[i], return_data);
        assert(status == 0);
        assert(return_data==data);
	printf("\rGet Done %d",i);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if(BASKET_CONF->MPI_RANK == 0) printf("\n");
    MPI_Barrier(MPI_COMM_WORLD);
    status = client->Destroy(j_name);
    assert(status == 0);
    MPI_Finalize();
    return 0;
}
