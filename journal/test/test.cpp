//
// Created by hariharan on 8/15/19.
//

#include <basket/common/macros.h>
#include <c++/journal/client/client.h>
#include <chronolog/common/configuration_manager.h>

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    int  num_events=1000;
    size_t size_events=4096;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 0);
    if(argc > 3) size_events=std::strtol(argv[3], nullptr, 0);
    CHRONOLOG_CONF->ConfigureJournalClient();
    auto client = basket::Singleton<journal::Client>::GetInstance();
    char journal_name[20];
    snprintf(journal_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(journal_name);
    int status = client->Create(j_name);
    assert(status == 0);
    Event e1,e2,e3;
    e1.journal_name_=CharStruct(journal_name);
    e1.event_id=1+BASKET_CONF->MPI_RANK*10;
    e2.journal_name_=CharStruct(journal_name);
    e2.event_id=2+BASKET_CONF->MPI_RANK*10;
    e3.journal_name_=CharStruct(journal_name);
    e3.event_id=3+BASKET_CONF->MPI_RANK*10;
    std::string data="Hello World";
    status = client->Append(e1,data);
    assert(status == 0);
    status = client->Append(e2,data);
    assert(status == 0);
    status = client->Append(e3,data);
    assert(status == 0);
    Event tail;
    std::string tail_data;
    tail.journal_name_=CharStruct(journal_name);
    status = client->Tail(tail,tail_data);
    assert(status == 0);
    assert(tail == e3);
    assert(tail_data == data);

    std::string ret_data;
    status = client->Retrieve(e2,ret_data);
    assert(status == 0);
    assert(ret_data == data);
    auto events = std::vector<std::pair<Event, std::string>>();
    status = client->RetrieveRange(e2.journal_name_,e2.event_id,e3.event_id,events);
    assert(status == 0);
    assert(events.size() == 2);
    for(auto result:events){
        assert(result.second==data);
    }
    Event oldest_event;
    uint16_t key_int = (uint16_t)std::hash<Event>()(e1)% BASKET_CONF->NUM_SERVERS;
    status = client->RetrieveOldestEntry(j_name,oldest_event,key_int);
    assert(status==0);
    assert(oldest_event == e1);
    status = client->DeleteEntry(e1);
    assert(status==0);
    status = client->DeleteEntry(e2);
    assert(status==0);
    status = client->DeleteEntry(e3);
    assert(status==0);
    MPI_Finalize();
    return 0;
}