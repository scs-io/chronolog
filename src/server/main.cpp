//
// Created by hariharan on 8/14/19.
//

#ifndef HLOG_SERVER_MAIN_H
#define HLOG_SERVER_MAIN_H


#include <mpi.h>
#include <basket/common/macros.h>
#include <c++/chronolog/server/server.h>
#include <chronolog/common/configuration_manager.h>
#include <chronolog/common/daemonize.h>

void finalize(){
    MPI_Barrier(MPI_COMM_WORLD);
    auto server = basket::Singleton<chronolog::Server>::GetInstance();
    server->Stop();
    MPI_Finalize();
}

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    /*if(BASKET_CONF->MPI_RANK==0){
        printf("%d ready for attach\n", BASKET_CONF->COMM_SIZE);
        fflush(stdout);
        getchar();
    }*/
    MPI_Barrier(MPI_COMM_WORLD);
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE = argv[1];
    BASKET_CONF->BACKED_FILE_DIR=CHRONOLOG_CONF->CHRONOLOG_DIR;
    CHRONOLOG_CONF->ConfigureChronologServer();
    auto server = basket::Singleton<chronolog::Server>::GetInstance();
    server->Run();
    signal(SIGTERM,signal_handler); /* catch kill signal */
    while(true) sleep(1);
}
#endif //HLOG_SERVER_MAIN_H
