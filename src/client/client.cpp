//
// Created by hariharan on 8/14/19.
//

#include <c++/chronolog/client/client.h>

int chronolog::Chronicle::Create(CharStruct &chronicle_name) {

    return djournal_client->Create(chronicle_name);
}

int chronolog::Chronicle::Destroy(CharStruct &chronicle_name) {
    return djournal_client->Destroy(chronicle_name);;
}

int chronolog::Chronicle::Open(CharStruct &chronicle_name) {
    return djournal_client->Open(chronicle_name);
}

int chronolog::Chronicle::Close(CharStruct &chronicle_name) {
    return djournal_client->Close(chronicle_name);
}

int chronolog::Chronicle::Append(Event &event, std::string &data) {
    return djournal_client->Append(event,data);
}

int chronolog::Chronicle::Tail(Event &event, std::string &data) {
    event.event_id = 0;
    int status = djournal_client->Tail(event,data);
    if(status == 0) return status;
    else if(event.event_id == 0) return status;
    return Retrieve(event,data);
}

int chronolog::Chronicle::Retrieve(Event &event, std::string &data) {
    int status= djournal_client->Retrieve(event,data);
    if(status < 0){
        auto iter = pkv_client->Get(event);
        if(!iter.first){
            Location location;
            status = metadata_client->Get(event, location);
            if(status < 0) return -1;
            io_client->Read(location,data);
        }
    }
    return 0;
}

int chronolog::Chronicle::RetrieveRange(CharStruct &chronicle_name, uint32_t start_event, uint32_t end_event, std::vector<std::pair<Event, std::string>> &events) {
    int status= djournal_client->RetrieveRange(chronicle_name,start_event,end_event,events);
    if(events.size() > 0 && events.rbegin()->first.event_id >=end_event)
        return 0;
    Event e_start,e_end;
    e_start.journal_name_=chronicle_name;
    e_end.journal_name_=chronicle_name;
    if(events.size() == 0) e_start.event_id=start_event;
    else e_start.event_id = events.rbegin()->first.event_id+1;
    e_end.event_id=end_event;
    auto ssd_data = pkv_client->GetRange(e_start,e_end);
    events.insert(events.end(),ssd_data.begin(),ssd_data.end());
    if(events.size() == 0) e_start.event_id=start_event;
    else e_start.event_id = events.rbegin()->first.event_id+1;
    auto file_events=std::vector<std::pair<Event, Location>>();
    status = metadata_client->GetRange(e_start,e_end,file_events);
    if(status > 0){
        for(auto file_event : file_events){
            std::string data;
            io_client->Read(file_event.second,data);
            events.emplace_back(file_event.first,data);
        }
    }
    sort(events.begin(), events.end(), [](const std::pair<Event, std::string>& a, const std::pair<Event, std::string>& b) -> bool
    {
        return a.first < b.first;
    });
    auto p = unique(events.begin(), events.end(), [](std::pair<Event, std::string> a, std::pair<Event, std::string> b)
    {
        return a.first == b.first;
    });
    events.resize(distance(events.begin(), p));
    return 0;
}

int chronolog::Chronicle::RetrieveRangeChronoplayer(CharStruct &chronicle_name, uint32_t start_event, uint32_t end_event, std::vector<std::pair<Event, std::string>> &events) {
    auto event = PlaybackEvent();
    event.journal_name_=chronicle_name;
    event.start_=start_event;
    event.end_=end_event;
    events = chronoplayer_client->GetRange(event);
    return 0;
}

int chronolog::Chronicle::RetrieveRangeAndSendChronoplayer(PlaybackEvent &representative, CharStruct &chronicle_name, uint32_t start_event, uint32_t end_event) {
    auto events = std::vector<std::pair<Event, std::string>>();
    int status = this->RetrieveRange(chronicle_name,start_event,end_event,events);
    if(status != 0) return status;
    status = chronoplayer_client->SendData(representative,events);
    return status;
}


int chronolog::Metadata::Put(Event &event, Location &location) {
    return metadata.Put(event,location)?0:-1;
}

int chronolog::Metadata::Get(Event &event, Location &location) {
    auto iter = metadata.Get(event);
    if(iter.first){
        location = iter.second;
    }else return -1;
    return 0;
}

int chronolog::Metadata::GetRange(Event &start_event, Event &end_event, std::vector<std::pair<Event, Location>> &events) {
    events = metadata.Contains(start_event,end_event);
    return events.size() > 0 ? 0:-1;
}

int chronolog::Metadata::Delete(Event &event) {
    auto iter = metadata.Erase(event);
    return iter.first?0:-1;
}
