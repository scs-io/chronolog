//
// Created by hariharan on 8/14/19.
//

#ifndef HLOG_SERVER_H
#define HLOG_SERVER_H


#include <chronolog/common/data_structure.h>
#include <c++/journal/common/common.h>
#include <basket/map/map.h>

namespace chronolog{
    class Server {
    private:
        basket::map<Event,Location> metadata;
        std::promise<void> exit_server_signal;
        std::thread worker;
        void RunInternal(std::future<void> futureObj){
            bool count=true;
            while(futureObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
                usleep(10000);
                if(count){
                    if(BASKET_CONF->MPI_RANK == 0) printf("Started the Chronolog server\n");
                    count=false;
                }
            }
        }
    public:
        Server(): metadata("FILE_META"),exit_server_signal(),worker(){}
        void Run(){
            std::future<void> futureObj = exit_server_signal.get_future();
            worker=std::thread (&Server::RunInternal, this, std::move(futureObj));
        }
        void Stop(){
            exit_server_signal.set_value();
            worker.join();
            if(BASKET_CONF->MPI_RANK == 0) printf("Stopped the Chronolog server\n");
        }

    };
}



#endif //HLOG_SERVER_H
