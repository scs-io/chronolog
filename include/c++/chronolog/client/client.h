//
// Created by hariharan on 8/14/19.
//

#ifndef HLOG_CLIENT_H
#define HLOG_CLIENT_H

#include <basket/common/data_structures.h>
#include <chronolog/common/data_structure.h>
#include <c++/chronoplayer/client/client.h>
#include <c++/journal/client/client.h>
#include <c++/pkv/client/client.h>
#include <basket/map/map.h>
#include <c++/pwriter/pwriter.h>
#include <chronolog/common/configuration_manager.h>

namespace chronolog {
    class Metadata{
    private:
        basket::map<Event,Location> metadata;
    public:
        Metadata(): metadata("FILE_META"){}
        int Put(Event& event, Location& location);
        int Get(Event& event, Location& location);
        int GetRange(Event& start_event,Event& end_event, std::vector<std::pair<Event,Location>> &events);
        int Delete(Event& event);
    };

    class Chronicle {
        std::shared_ptr<journal::Client> djournal_client;
        std::shared_ptr<chronoplayer::Client> chronoplayer_client;
        std::shared_ptr<pkv::Client> pkv_client;
        std::shared_ptr<chronolog::Metadata> metadata_client;
        std::shared_ptr<pwriter::ParallelWriter> io_client;
    public:
        Chronicle(){
            CHRONOLOG_CONF->ConfigureJournalClient();
            djournal_client=basket::Singleton<journal::Client>::GetInstance();

            CHRONOLOG_CONF->ConfigureChronoplayerClient();
            chronoplayer_client=basket::Singleton<chronoplayer::Client>::GetInstance();

            CHRONOLOG_CONF->ConfigurePKVClient();
            pkv_client=basket::Singleton<pkv::Client>::GetInstance("SSD_KV");

            CHRONOLOG_CONF->ConfigureChronologClient();
            metadata_client=basket::Singleton<chronolog::Metadata>::GetInstance();
            io_client=basket::Singleton<pwriter::ParallelWriter>::GetInstance();
        }
        int Create(CharStruct& chronicle_name);
        int Destroy(CharStruct& chronicle_name);
        int Open(CharStruct& chronicle_name);
        int Close(CharStruct& chronicle_name);
        int Append(Event& event, std::string &data);
        int Tail(Event& event,std::string &data);
        int Retrieve(Event& event, std::string &data);
        int RetrieveRange(CharStruct& chronicle_name, uint32_t start_event, uint32_t end_event, std::vector<std::pair<Event,std::string>> &events);
        int RetrieveRangeChronoplayer(CharStruct& chronicle_name, uint32_t start_event, uint32_t end_event, std::vector<std::pair<Event,std::string>> &events);
        int RetrieveRangeAndSendChronoplayer(PlaybackEvent &representative, CharStruct& chronicle_name, uint32_t start_event, uint32_t end_event);

    };


}


#endif //HLOG_CLIENT_H
