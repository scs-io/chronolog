//
// Created by hariharan on 8/14/19.
//

#ifndef HLOG_PWRITER_H
#define HLOG_PWRITER_H


#include <chronolog/common/data_structure.h>

namespace pwriter{
    class ParallelWriter {
    private:
    public:
        ParallelWriter(){}
        int Append(Location &location, std::string& data);
        int Read(Location &location,std::string& data);
    };
}



#endif //HLOG_PWRITER_H
