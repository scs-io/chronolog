//
// Created by hariharan on 8/14/19.
//

#include <c++/pwriter/pwriter.h>

int pwriter::ParallelWriter::Append(Location &location, std::string &data) {
    FILE* file=fopen(location.path.c_str(),"a+");
    fwrite(data.c_str(),sizeof(char),location.event_size,file);
    fclose(file);
    return 0;
}

int pwriter::ParallelWriter::Read(Location &location, std::string &data) {
    FILE* file=fopen(location.path.c_str(),"r");
    fseek(file,location.offset,SEEK_SET);
    char * data_ptr= static_cast<char *>(malloc(location.event_size));
    fread(data_ptr,sizeof(char),location.event_size,file);
    data.assign(data_ptr,0,location.event_size);
    free(data_ptr);
    fclose(file);
    return 0;
}
