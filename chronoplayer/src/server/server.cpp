//
// Created by hariharan on 11/6/19.
//

#include <chronolog/common/configuration_manager.h>
#include "../../include/c++/chronoplayer/server/server.h"

chronoplayer::Server::Server():server_request_queue("SERVER_REQUEST_QUEUE",CHRONOLOG_CONF->CHRONOPLAYER_SERVER_PORT) {
    server_rpc=server_request_queue.rpc;
    std::function<std::vector<PlaybackEvent>(void)> functionGetRangeRequests(std::bind(&chronoplayer::Server::GetRangeRequests,this));
    server_rpc->bind("GetRangeRequests", functionGetRangeRequests);
}
void chronoplayer::Server::RunInternal(std::future<void> futureObj) {
    bool count=true;
    while(futureObj.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout){
        usleep(10000);
        if(count){
            if(BASKET_CONF->MPI_RANK == 0) printf("Started the Chronoplayer server\n");
            count=false;
        }
    }
}

std::vector<PlaybackEvent> chronoplayer::Server::GetRangeRequests() {
    auto num_elements =  server_request_queue.LocalSize();
    auto events = std::vector<PlaybackEvent>();
    int count=0;
    while(count < num_elements && server_request_queue.LocalSize() != 0){
        auto ele = server_request_queue.LocalPop();
        if(ele.first) events.push_back(ele.second);
    }
    return events;
}
