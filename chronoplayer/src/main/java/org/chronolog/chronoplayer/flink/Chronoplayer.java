package org.chronolog.chronoplayer.flink;

import jchronolog.utils.CustomPlaybackEvent;
import jchronolog.utils.Event;
import jchronolog.utils.Pair;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.chronolog.chronoplayer.flink.operators.ChronoplayerSource;
import org.chronolog.chronoplayer.flink.operators.PlaybackReader;
import org.chronolog.chronoplayer.flink.operators.CustomDeduplicator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.ArrayList;

public class Chronoplayer {

	public static void main(String[] args) throws Exception {
		JSONParser parser = new JSONParser();

		FileReader fileReader;
		if(args.length == 0){
			fileReader = new FileReader("config.json");
		}
		else {
			fileReader = new FileReader(args[0]);
		}
		JSONObject json = (JSONObject) parser.parse(fileReader);

		int max_chronoplayer_servers = ((Long)json.get("max_servers")).intValue();
		int max_number_range = ((Long)json.get("max_number_range")).intValue();
		int max_timeout_range = ((Long)json.get("max_timeout_range")).intValue();
		int range_len = ((Long)json.get("range_len")).intValue();
		String conf_path = (String) json.get("conf_path");

		System.out.println("");
		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		DataStream<CustomPlaybackEvent> rangeEntriesSources=env.addSource(new ChronoplayerSource(0,range_len, conf_path));
		for(int i=1;i<max_chronoplayer_servers;++i){
			rangeEntriesSources = rangeEntriesSources.union(env.addSource(new ChronoplayerSource(i,range_len, conf_path)));
		}
		KeyedStream<CustomPlaybackEvent, Integer> playbackEntries = rangeEntriesSources.keyBy(event -> event.hashCode());
		KeyedStream<Pair<Integer, ArrayList<CustomPlaybackEvent>>, Integer> deduplicatedEntries = playbackEntries.process(new CustomDeduplicator(max_number_range, max_timeout_range)).keyBy(event -> event.getFirst());

		deduplicatedEntries.addSink(new PlaybackReader());
		env.execute("Chronoplayer");
	}
}