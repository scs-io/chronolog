package org.chronolog.chronoplayer.flink.operators;

import jchronolog.chronoplayer.Chronoplayer;
import jchronolog.pkv.PKV;
import jchronolog.utils.CustomPlaybackEvent;
import jchronolog.utils.Event;
import jchronolog.utils.PlaybackEvent;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

import java.util.ArrayList;

public class ChronoplayerSource extends RichSourceFunction<CustomPlaybackEvent> {

    private Chronoplayer chronoplayerClient;

    private PKV pkvClient;
    private int count;
    private int server_index;
    private Integer len;
    private String conf_path;

    public ChronoplayerSource(int server_index,Integer len, String conf_path) {
        this.server_index=server_index;
        this.len=len;
        this.conf_path=conf_path;
    }

    public void open(Configuration parameters) throws InterruptedException {
        /**
         * instantiate client
         */
        chronoplayerClient = new Chronoplayer();
        /**
         * MPI_init
         */
        chronoplayerClient.Init(conf_path);
        chronoplayerClient.InitChronoplayer();
    }

    @Override
    public void run(SourceContext<CustomPlaybackEvent> sourceContext) {
        Event msg=new Event();
        msg.event_id=0;
        int count=0;
        while(true) {
            try {
                Thread.sleep(100);
                ArrayList<PlaybackEvent> oldest_datas = chronoplayerClient.GetRangeRequests(server_index);
                if(oldest_datas.size() == 0) continue;
                System.out.println("Got playbacks: " + oldest_datas.size());

                for(PlaybackEvent playbackEvent :oldest_datas){
                    int start_=playbackEvent.startEventId;
                    while(start_ < playbackEvent.endEventId){
                        Integer end_event_id = (start_/len + 1)*len - 1;
                        if(end_event_id > playbackEvent.endEventId) end_event_id = playbackEvent.endEventId;
                        CustomPlaybackEvent customPlaybackEvent=new CustomPlaybackEvent(playbackEvent,start_,end_event_id,len);
                        sourceContext.collect(customPlaybackEvent);
                        start_ = end_event_id + 1;
                    }
                }
            }
            catch(Exception e){
                count++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                if(count > 5){
                    System.out.println("closing");
                    break;
                }
            }
        }
    }

    @Override
    public void cancel() {
        try{
            chronoplayerClient.Exit();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
    }
}