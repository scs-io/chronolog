package org.chronolog.chronoplayer.flink.operators;

import jchronolog.utils.CustomPlaybackEvent;
import jchronolog.utils.Pair;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;

public class CustomDeduplicator extends KeyedProcessFunction<Integer, CustomPlaybackEvent, Pair<Integer, ArrayList<CustomPlaybackEvent>>> {
    private ValueState<ArrayList<CustomPlaybackEvent>> eventList = null;
    private ValueState<Long> timerTime = null;

    private int max_size;
    private int max_timeout;

    public CustomDeduplicator(int max_size, int max_timeout){
        this.max_size = max_size;
        this.max_timeout = max_timeout;
    }

    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<ArrayList<CustomPlaybackEvent>> descriptor = new ValueStateDescriptor<>(
                "sorted-events",
                TypeInformation.of(new TypeHint<ArrayList<CustomPlaybackEvent>>() {
                }));
        eventList = getRuntimeContext().getState(descriptor);

        ValueStateDescriptor<Long> descriptorLong = new ValueStateDescriptor<>(
                "Time_Timer",
                TypeInformation.of(new TypeHint<Long>() {
                }));
        timerTime = getRuntimeContext().getState(descriptorLong);
    }

    @Override
    public void processElement(CustomPlaybackEvent event, Context ctx, Collector<Pair<Integer, ArrayList<CustomPlaybackEvent>>> collector) throws Exception {
        TimerService timerService = ctx.timerService();
        Long coalescedTime;
        ArrayList<CustomPlaybackEvent> list = eventList.value();
        if (list == null) {
            list = new ArrayList<>();
            coalescedTime = System.currentTimeMillis() + max_timeout;
            timerTime.update(coalescedTime);
            ctx.timerService().registerProcessingTimeTimer(coalescedTime);
        }

        list.add(event);
        eventList.update(list);

        if(list.size() >= max_size){
            coalescedTime = timerTime.value();
            ctx.timerService().deleteProcessingTimeTimer(coalescedTime);

            eventList.update(null);
            collector.collect(new Pair<Integer, ArrayList<CustomPlaybackEvent>>(list.get(0).hashCode(), list));
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext context, Collector<Pair<Integer, ArrayList<CustomPlaybackEvent>>> out) throws Exception {
        ArrayList<CustomPlaybackEvent> list = eventList.value();

        if(list.size() > 0) {
            eventList.update(null);
            out.collect(new Pair<Integer, ArrayList<CustomPlaybackEvent>>(list.get(0).hashCode(), list));
        }
    }
}
