package org.chronolog.chronoplayer.flink.operators;

import jchronolog.chronolog.Chronolog;
import jchronolog.utils.CustomPlaybackEvent;
import jchronolog.utils.Pair;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;

import java.io.IOException;
import java.util.ArrayList;

public class PlaybackReader extends RichSinkFunction<Pair<Integer, ArrayList<CustomPlaybackEvent>>> {
    private Chronolog chronologClient;
    private jchronolog.chronoplayer.Chronoplayer chronoplayerClient;

    public PlaybackReader(){}

    @Override
    public void open(Configuration config) {
        chronologClient =new Chronolog();
        chronoplayerClient = new jchronolog.chronoplayer.Chronoplayer();
    }

    @Override
    public void invoke(Pair<Integer, ArrayList<CustomPlaybackEvent>> event, SinkFunction.Context context) throws IOException, InterruptedException {
        ArrayList<CustomPlaybackEvent> deduplicatedList = event.getSecond();
        for (CustomPlaybackEvent customPlaybackEvent : deduplicatedList) {
            customPlaybackEvent.original.startEventId = customPlaybackEvent.start_;
            customPlaybackEvent.original.endEventId = customPlaybackEvent.end_;
            System.out.println("Pushing events between "+customPlaybackEvent.start_+" "+customPlaybackEvent.end_);
            chronologClient.ChronicleRetrieveRangeAndSendChronoplayer(customPlaybackEvent.original, customPlaybackEvent.original.journalName, customPlaybackEvent.start_, customPlaybackEvent.end_);
        }
    }
}
