//
// Created by hariharan on 11/6/19.
//

#include <chronolog/common/configuration_manager.h>
#include "../../include/c++/chronoplayer/client/client.h"

chronoplayer::Client::Client(bool requester):playbackData(),
                                             server_request_queue("SERVER_REQUEST_QUEUE"),
                                             num_servers(BASKET_CONF->NUM_SERVERS),
                                             current_node(){
    
    
    if(requester){
        int len;
        char proc_name[MPI_MAX_PROCESSOR_NAME]="localhost";
        //MPI_Get_processor_name(proc_name, &len);
        CHRONOLOG_CONF->ConfigureChronoplayerClientServer();
        current_node.node_name_ = CharStruct(proc_name);
        current_node.port_=CHRONOLOG_CONF->CHRONOPLAYER_CLIENT_PORT+BASKET_CONF->MPI_RANK;
        client_rpc=basket::Singleton<RPCFactory>::GetInstance()->GetRPC(current_node.port_);
        CHRONOLOG_CONF->ConfigureChronoplayerClient();
        std::function<int(PlaybackEvent&,std::vector<std::pair<Event, std::string>>&)> functionGetRangeCallBack(std::bind(&chronoplayer::Client::GetRangeCallBack,this,
                std::placeholders::_1,std::placeholders::_2));
        client_rpc->bind("GET_RANGE_CALLBACK",functionGetRangeCallBack);
    }
}

std::vector<std::pair<Event, std::string>> chronoplayer::Client::GetRange(PlaybackEvent &event) {
    event.destination = current_node;
    uint16_t server_index=rand()%num_servers;
    playbackData.clear();
    server_request_queue.Push(event,server_index);
    original_event=event;
    while(!hasDataArrived()) usleep(10);
    return collectArrivedData();
}

std::vector<PlaybackEvent> chronoplayer::Client::GetRangeRequests(uint16_t &server_index) {
    auto server_rpc = basket::Singleton<RPCFactory>::GetInstance()->GetRPC(CHRONOLOG_CONF->CHRONOPLAYER_SERVER_PORT);
    return server_rpc->callWithTimeout<RPCLIB_MSGPACK::object_handle>(server_index,2000, "GetRangeRequests").as<std::vector<PlaybackEvent>>();
}

int chronoplayer::Client::SendData(PlaybackEvent &representative, std::vector<std::pair<Event, std::string>> &events) {
    auto client_rpc=basket::Singleton<RPCFactory>::GetInstance()->GetRPC(representative.destination.port_);
    return client_rpc->call<RPCLIB_MSGPACK::object_handle>(representative.destination.node_name_,representative.destination.port_,"GET_RANGE_CALLBACK",representative,events).as<int>();;
}

int chronoplayer::Client::GetRangeCallBack(PlaybackEvent &representative, std::vector<std::pair<Event, std::string>> &events) {
    mutex.lock();
    playbackData.insert(std::pair<PlaybackEvent,std::vector<std::pair<Event, std::string>>>(representative,events));
    mutex.unlock();
    return 0;
}

bool chronoplayer::Client::hasDataArrived() {
    uint32_t start=0,end=0;
    auto iter = playbackData.begin();

    while(iter != playbackData.end()){
        int size=playbackData.size();
        if(iter == playbackData.begin()){
            start=iter->first.start_;
            end=iter->first.end_;
        }else{
            if(end != iter->first.start_-1) return false;
            else end = iter->first.end_;
        }
        iter++;
    }
    if(start == 0) return false;
    return start == original_event.start_ && end == original_event.end_;
}

std::vector<std::pair<Event, std::string>> chronoplayer::Client::collectArrivedData() {
    auto events = std::vector<std::pair<Event, std::string>>();
    auto iter = playbackData.begin();
    while(iter!=playbackData.end()){
        events.insert(events.end(),iter->second.begin(),iter->second.end());
        iter++;
    }
    sort(events.begin(), events.end(), [](const std::pair<Event, std::string>& a, const std::pair<Event, std::string>& b) -> bool
    {
        return a.first < b.first;
    });
    auto p = unique(events.begin(), events.end(), [](std::pair<Event, std::string> a, std::pair<Event, std::string> b)
    {
        return a.first == b.first;
    });
    events.resize(distance(events.begin(), p));
    return events;
}
