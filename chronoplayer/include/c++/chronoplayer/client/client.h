//
// Created by hariharan on 11/6/19.
//

#ifndef CHRONOLOG_CLIENT_H
#define CHRONOLOG_CLIENT_H

#include <chronolog/common/data_structure.h>
#include <basket/queue/queue.h>
#include <boost/interprocess/containers/string.hpp>
#include <boost/thread.hpp>


namespace bip=boost::interprocess;

namespace chronoplayer{
    class Client {
    private:
        basket::queue<PlaybackEvent>  server_request_queue;
        std::shared_ptr<RPC> client_rpc;
        std::map<PlaybackEvent,std::vector<std::pair<Event,std::string>>> playbackData;
        boost::mutex mutex;
        PlaybackEvent original_event;
        int num_servers;
        Node current_node;

        int GetRangeCallBack(PlaybackEvent &representative,std::vector<std::pair<Event,std::string>>& events);
        bool hasDataArrived();
        std::vector<std::pair<Event, std::string>> collectArrivedData();
    public:
        Client(bool requester=true);
        std::vector<std::pair<Event,std::string>> GetRange(PlaybackEvent& event);
        std::vector<PlaybackEvent> GetRangeRequests(uint16_t &server_index);
        int SendData(PlaybackEvent &representative, std::vector<std::pair<Event,std::string>>& events);
    };
}



#endif //CHRONOLOG_CLIENT_H
