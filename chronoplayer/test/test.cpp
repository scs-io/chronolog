//
// Created by hariharan on 11/14/19.
//

#include <mpi.h>
#include <chronolog/common/configuration_manager.h>
#include <c++/chronoplayer/client/client.h>

int main(int argc, char* argv[]){
    MPI_Init(&argc,&argv);
    int  num_events=1000;
    size_t size_events=4096;
    if(argc > 1) CHRONOLOG_CONF->CONFIGURATION_FILE=argv[1];
    if(argc > 2) num_events=(int)std::strtol(argv[2], nullptr, 0);
    if(argc > 3) size_events=std::strtol(argv[3], nullptr, 0);
    CHRONOLOG_CONF->ConfigureChronoplayerClient();
    auto chronoplayer_client = basket::Singleton<chronoplayer::Client>::GetInstance();
    char journal_name[20];
    snprintf(journal_name, 20, "test_%d",BASKET_CONF->MPI_RANK);
    CharStruct j_name=CharStruct(journal_name);
    auto event=PlaybackEvent();
    event.journal_name_=j_name;
    event.start_=0;
    event.end_=10;
    auto events = chronoplayer_client->GetRange(event);
    return 0;
}